#!/bin/bash

# Define the paths
SERVICE_FILE="/etc/systemd/system/canbus.service"
SCRIPT_FILE="/usr/local/bin/canbus_start.sh"
UENV_FILE="/boot/uEnv.txt"

# Copy the service file
echo "Copying canbus.service to /etc/systemd/system/"
sudo cp canbus.service "$SERVICE_FILE"

# Copy the start script
echo "Copying canbus_start.sh to /usr/local/bin/"
sudo cp canbus_start.sh "$SCRIPT_FILE"

# Make the script executable
echo "Making canbus_start.sh executable"
sudo chmod +x "$SCRIPT_FILE"

# Modify uEnv.txt to include CAN bus device tree overlay
echo "Checking and updating uEnv.txt for CAN bus overlay"

# Check and modify uboot_overlay_addr0
if grep -q "^uboot_overlay_addr0=" "$UENV_FILE"; then
    echo "uboot_overlay_addr0 already exists in uEnv.txt. Modifying it."
    sudo sed -i 's/^uboot_overlay_addr0=.*/uboot_overlay_addr0=\/lib\/firmware\/BB-CAN1-00A0.dtbo/' "$UENV_FILE"
else
    echo "uboot_overlay_addr0 does not exist. Adding it to uEnv.txt."
    echo "uboot_overlay_addr0=/lib/firmware/BB-CAN1-00A0.dtbo" | sudo tee -a "$UENV_FILE"
fi

# Check and add enable_uboot_overlays if not present
if ! grep -q "^enable_uboot_overlays=1" "$UENV_FILE"; then
    echo "enable_uboot_overlays=1 is not set. Adding it to uEnv.txt."
    echo "enable_uboot_overlays=1" | sudo tee -a "$UENV_FILE"
else
    echo "enable_uboot_overlays=1 already exists in uEnv.txt."
fi

# Reload the systemd daemon to recognize the new service
echo "Reloading systemd daemon"
sudo systemctl daemon-reload

# Enable the service to start on boot
echo "Enabling canbus service"
sudo systemctl enable canbus.service

# Start the service immediately
echo "Starting canbus service"
sudo systemctl start canbus.service

echo "Installation complete. CAN bus service is now active and will start on boot."
