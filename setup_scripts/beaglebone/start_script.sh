#!/bin/bash
cd /home/debian/can_gateway/11_serverV2
echo "$(date) - Starting script" >> /tmp/service_start.log

/usr/bin/screen -DmS can_gateway ./run_python.sh

echo "$(date) - Reached end of script" >> /tmp/service_start.log