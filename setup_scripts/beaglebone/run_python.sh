#!/bin/bash
cd /home/debian/git/rehmtech/can_gateway/11_serverV2
while true; do
    python3 the_core.py # > /tmp/the_core.log 2>&1
    exit_status=$?

    if [ $exit_status -ne 0 ]; then
        echo "$(date) - The core.py exited with status $exit_status, restarting in 10 seconds..." >> /tmp/service_start.log
        sleep 10
    else
        echo "$(date) - The core.py exited successfully." >> /tmp/service_start.log
        break
    fi
done
