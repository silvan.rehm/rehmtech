from ea_psu_controller import PsuEA
import time

psu = PsuEA(comport='ttyACM0')

psu.remote_on()
psu.set_voltage(24)
psu.set_current(1)
psu.output_on()


time.sleep(5)


psu.close()