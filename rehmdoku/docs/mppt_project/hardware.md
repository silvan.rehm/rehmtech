# Connections

The MPPT mk6 has two connectors: 

- Power [digikey:277-1013-ND](https://www.digikey.ch/product-detail/de/phoenix-contact/1757035/277-1013-ND/260381)
	+ Left side is for PV Panel
	+ Right side is for DC Link 
	+ **DC Link needs to be fused**
- Auxilliary [digikey:102-6470-ND](https://www.digikey.ch/product-detail/de/cui-devices/TBP02P1-381-04BE/102-6470-ND/10238447)
	+ Input voltage: 12..24V
	+ No Termination Resistor on the CAN Bus

(GND1, GND2 and GND3 are internally connected)

![usage](assets/usage.png)



# Cooling

No active cooling required for current configuration (5A max).




# CAN Bus

## reporting

The PCB send out the following messages every second:

- high side values:
	+ id: AA0000 | node_id (i.e. AA0008 for node 08)
	+ values:
		* byte 0..3: voltage
		* byte 4..7: current
- low side values:
	+ id: AA0100 | node_id (i.e. AA0108 for node 08)
	+ values:
		* byte 0..3: voltage
		* byte 4..7: current
		
values 32 bit floats:

```cpp
char *b = (char*) &V_Hi;
for(int i = 0; i < 4; i++)
    msg_buffer[i] = b[i];
```

to get the float back in python:

```python
_data = msg.data.hex()
_float = _struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
```

## controlling

to be done
