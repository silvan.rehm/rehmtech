import socket
import time

class PPA1500:
    parameters = [
    "freq", "rms", "mag", "dc", "phase", "pk", "cf", "mean", "form factor", "harm"
]
    def __init__(self, IP = '192.168.1.101', PORT = 10001):
        self.IP = IP
        self.PORT = PORT
        
    def get_IDN(self):
        idn_command = "*IDN?"
        response = self.send_scpi_command(idn_command) 
        return response

    def setup_and_start(self):
        cmd = "MODE,POWER"
        response = self.send_scpi_command(cmd)
 

        cmd = "POWER,AVERAGE"
        response = self.send_scpi_command(cmd)


        cmd = "START"
        response = self.send_scpi_command(cmd)


    def wait_for_measurement(self):
        cmd = "*OPC?"
        response = self.send_scpi_command(cmd).strip()
        while str(response) != "1" :
            cmd = "*OPC?"
            response = self.send_scpi_command(cmd).strip()
            # print(f"{cmd}: |{response}|")
            time.sleep(0.1)

    def get_values(self):
        parameters = self.parameters
        data = {
            "PHASE1": {},
            "PHASE2": {},
            "PHASE3": {}
        }
        # Read current measurements for all phases
        cmd = "POWER,PHASES,CURRENT?"
        response = self.send_scpi_command(cmd)
  

        # Split the response into individual phase measurements
        values = [float(value) for value in response.split(',')]
        phase_count = len(data)
        values_per_phase = len(parameters)

        # Assign the values to the respective phases
        for i, phase in enumerate(data.keys()):
            start_index = i * values_per_phase
            end_index = start_index + values_per_phase
            data[phase]['current'] = dict(zip(parameters, values[start_index:end_index]))

        # Read voltage measurements for all phases
        cmd = "POWER,PHASES,VOLTAGE?"
        response = self.send_scpi_command(cmd)


        # Split the response into individual phase measurements
        values = [float(value) for value in response.split(',')]

        # Assign the values to the respective phases
        for i, phase in enumerate(data.keys()):
            start_index = i * values_per_phase
            end_index = start_index + values_per_phase
            data[phase]['voltage'] = dict(zip(parameters, values[start_index:end_index]))
        return data

    def send_scpi_command(self, command):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            # Connect to the PPA1500
            s.connect((self.IP, self.PORT))

            # Send the SCPI command
            s.sendall(command.encode('ascii') + b'\n')
            # time.sleep(0.5)

            # Receive the response if the command expects one
            if command.endswith("?"):
                response = ""
                while True:
                    chunk = s.recv(1024).decode('ascii')
                    response += chunk
                    if "\n" in chunk:
                        break
                response = response.strip()
            else:
                response = "None"

            return response
