import aux_calib as aux
import yaml
import numpy as np
from PPA1500 import PPA1500
import pandas as pd

from ea_psu_controller import PsuEA
import time

psu = PsuEA(comport='ttyACM0')

psu.remote_on()
psu.set_voltage(0)
psu.set_current(0)

csv_fn = "/home/shed/cloud/NC_LH/10_RehmTech/40_Measurements/41_Calibrations/auto/mk6_00.csv"
steps = ['voltage', '8 Ohm', '4 Ohm', 'short']
vary = ['voltage', 'current', 'current', 'current']

#steps = ['voltage',  'short']
#vary = ['voltage', 'current']
#step = steps[0]
last_step_index = -1
step_index = 0
value_index = -1

voltages = [i for i in np.arange(0,42,2)]
current_during_voltage = 1

voltage_during_current = 24
currents = [i for i in np.arange(0,7,1/3)]

db = yaml.safe_load(open("config.yaml"))

pa = PPA1500(IP = '192.168.1.101', PORT = 10001)
print(pa.get_IDN())

# Number of measurements
N_meas = 5

# Initialize DataFrame

# Initialize DataFrame
columns = ['I_Lo_MCU', 'I_Lo_PA', 'I_Hi_MCU', 'I_Hi_PA', 'V_Lo_MCU', 'V_Lo_PA', 'V_Hi_MCU', 'V_Hi_PA']
df = pd.DataFrame(columns=columns)

# Main loop
while True:
    if step_index >= len(steps):
        print("DONE")
        break
    if step_index != last_step_index:
        psu.output_off()
        last_step_index = step_index
        print(f"prepare for {steps[step_index]}")
        resp = input("enter to confirm, q to quit")
        if resp == "q":
            break
        psu.output_on()

    if vary[step_index] == "voltage":
        print("varying voltage")
        value_index = value_index + 1
        if value_index >= len(voltages): # end
            value_index = -1
            step_index = step_index + 1
            continue
        print(f"voltage: setting {voltages[value_index]} V , {current_during_voltage} A ")
        
        psu.set_current(current_during_voltage)
        psu.set_voltage(voltages[value_index])

    elif vary[step_index] == "current":        
        value_index = value_index + 1
        if value_index >= len(currents): # end
            value_index = -1
            step_index = step_index + 1
            continue
        print(f"current: setting {currents[value_index]} A , {voltage_during_current} V ")
        psu.set_voltage(voltage_during_current)
        psu.set_current(currents[value_index])
        
    else:
        print(f"unkown: {vary[step_index]}")
    
    time.sleep(2)  # give it time to ramp up

    Is_Lo_MCU, Is_Lo_PA = [], []
    Is_Hi_MCU, Is_Hi_PA = [], []
    Vs_Lo_MCU, Vs_Lo_PA = [], []
    Vs_Hi_MCU, Vs_Hi_PA = [], []

    for i in range (N_meas):
        print(f"{i}/{N_meas}")
        aux.aux_connect_to_server(db)
        aux.get_update(db)
        aux.process_can_msgs(db)
        eng_data = db['can_ids'].get("0xee",{})        

        pa.setup_and_start()
        pa.wait_for_measurement()
        data = pa.get_values()

        Is_Lo_MCU.append(eng_data.get('I_Lo',0))
        Is_Lo_PA.append(data["PHASE1"]["current"]["rms"])

        Is_Hi_MCU.append(eng_data.get('I_Hi',0))
        Is_Hi_PA.append(data["PHASE2"]["current"]["rms"])

        Vs_Lo_MCU.append(eng_data.get('V_Lo',0))
        Vs_Lo_PA.append(data["PHASE1"]["voltage"]["rms"])

        Vs_Hi_MCU.append(eng_data.get('V_Hi',0))
        Vs_Hi_PA.append(data["PHASE2"]["voltage"]["rms"])

    I_Lo_MCU_med = np.median(Is_Lo_MCU)
    I_Lo_PA_med = np.median(Is_Lo_PA)
    I_Hi_MCU_med = np.median(Is_Hi_MCU)
    I_Hi_PA_med = np.median(Is_Hi_PA)
    V_Lo_MCU_med = np.median(Vs_Lo_MCU)
    V_Lo_PA_med = np.median(Vs_Lo_PA)
    V_Hi_MCU_med = np.median(Vs_Hi_MCU)
    V_Hi_PA_med = np.median(Vs_Hi_PA)

    # Add new entry to the DataFrame    
    # Add new entry to the DataFrame
    new_entry = pd.DataFrame({
        'I_Lo_MCU': [I_Lo_MCU_med], 'I_Lo_PA': [I_Lo_PA_med], 'I_Hi_MCU': [I_Hi_MCU_med], 'I_Hi_PA': [I_Hi_PA_med],
        'V_Lo_MCU': [V_Lo_MCU_med], 'V_Lo_PA': [V_Lo_PA_med], 'V_Hi_MCU': [V_Hi_MCU_med], 'V_Hi_PA': [V_Hi_PA_med]
    })
    df = pd.concat([df, new_entry], ignore_index=True)

    print(new_entry)

   
    #req = input("next value, press enter")
    #if req =="q":
    #   break
print(df)
psu.close()
df.to_csv(csv_fn)
#df_new, calib_data = aux.calc_calib_data(df)

