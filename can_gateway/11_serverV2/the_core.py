import json
import logging
import signal
import time
from queue import Queue

import yaml

import aux_core as aux
from can_gateway_classes import ClientHandler, Updater
from aggregator_PV import Aggregator_PV
from uploader import Uploader2
from i2cthread import I2CThread

def signal_handler(signal, frame):  # Ctrl + C handler
    aux.shut_down(db)


signal.signal(signal.SIGINT, signal_handler)

db = aux.load_config()
data_body = json.load(open("config/body.json"))
data_body['measurement'] = db['measurement']
aux.prepare_logging(db)

db['can_send_queue'] = Queue()
db['can_recv_queue'] = Queue()

upload_queue = Queue()

ch = ClientHandler(db)
ch.setDaemon(True)
ch.start()

ul = Updater(db)
ul.setDaemon(True)
ul.start()

aggregator = Aggregator_PV(db, db['can_recv_queue'], upload_queue, data_body)
aggregator.setDaemon(True)
aggregator.start()

#i2cthread = I2CThread(db['i2c_config'], upload_queue, data_body)
#i2cthread.setDaemon(True)
#i2cthread.start()

uploader = Uploader2(db, upload_queue)
uploader.setDaemon(True)
uploader.start()


while db['running']:
    # logging.info("server alive")
    time.sleep(1)

logging.info("server finished")
