import logging
import threading
import time
from queue import Queue
# from influxdb import InfluxDBClient
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS

class Uploader(threading.Thread):

    def __init__(self, config, upload_queue: Queue):
        threading.Thread.__init__(self, name="uploader")
        self.running = True
        self.upload_queue = upload_queue
        self.config = config['uploader']
        print(self.config)
        host = self.config['influx']['host']
        port = self.config['influx']['port']
        db_user_name = self.config['influx']['db_user_name']
        db_user_password = self.config['influx']['db_user_password']
        db_name = self.config['influx']['db_name']
        self.client = InfluxDBClient(host, port, db_user_name, db_user_password, db_name)
        logging.info("uploader init")

    def stop(self):
        logging.info("uploader stopped")
        self.running = False

    def upload(self):
        # logging.info("uploading!")
        items = []
        while not self.upload_queue.empty():
            items.append(self.upload_queue.get())
        print(items)
        self.client.write_points(items)

    def run(self):
        logging.info("uploader starting")
        while self.running:
            time.sleep(self.config['time'])
            try:
                self.upload()
            except Exception as e:
                print(e)

class Uploader2(threading.Thread):

    def __init__(self, config, upload_queue: Queue):
        threading.Thread.__init__(self, name="uploaderV2")
        self.running = True
        self.upload_queue = upload_queue
        self.config = config['uploader']
        print(self.config)    
        self.bucket = self.config['influx']['bucket']
        self.org = self.config['influx']['org']
        self.client = influxdb_client.InfluxDBClient(
            url=f"{self.config['influx']['host']}:{self.config['influx']['port']}",
            token=self.config['influx']['token'],
            org=self.config['influx']['org']
        )
        # self.client = InfluxDBClient(host, port, db_user_name, db_user_password, db_name)
        logging.info("uploader init")

    def stop(self):
        logging.info("uploader stopped")
        self.running = False

    def upload(self):
        # logging.info("uploading!")
        items = []
        write_api = self.client.write_api(write_options=SYNCHRONOUS)
        while not self.upload_queue.empty():
            item = self.upload_queue.get()
            print(item)
            item.pop('time')
            write_api.write(self.bucket, self.org, item)

            # items.append(self.upload_queue.get())
        # write_api.write(self.bucket, self.org, items)
        # print(items)
        
        
        # self.client.write_points(items)

    def run(self):
        logging.info("uploader starting")
        while self.running:
            time.sleep(self.config['time'])
            try:
                self.upload()
            except Exception as e:
                print(e)