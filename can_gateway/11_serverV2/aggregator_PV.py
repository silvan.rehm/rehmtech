import logging
import struct
import threading
import time
from copy import deepcopy
from datetime import datetime
from queue import Queue


class Aggregator_PV(threading.Thread):

    def __init__(self, config, can_queue: Queue, upload_queue: Queue, data_body):
        threading.Thread.__init__(self, name="aggregator_PV")
        self.running = True
        self.can_queue = can_queue
        self.upload_queue = upload_queue
        self.config = config['aggregator_PV']
        self.data_body_template = data_body
        self.data_body_template['measurement'] = config['measurement'] #"PV-Data_test1"
        logging.info("aggregator init")

    @staticmethod
    def queue_get_all(q: Queue):
        items = []
        maxItemsToRetrieve = 10
        for numOfItemsRetrieved in range(0, maxItemsToRetrieve):
            try:
                if numOfItemsRetrieved == maxItemsToRetrieve:
                    break
                items.append(q.get_nowait())
            except:
                break
        return items

    def stop(self):
        logging.info("aggregator stopped")
        self.running = False

    def aggregate(self):
        # logging.info("aggregating!")
        items = []
        while not self.can_queue.empty():
            items.append(self.can_queue.get())

        # print(items)
        averages = {}
        for item in items:
            _id = item.arbitration_id
            _data = item.data.hex()
            _node_id = int(_id) & 0xFF
            node_id = "{0:#0{1}x}".format(_node_id, 4)  # into nicelooking strings (i.e. 0x0A)
            if node_id not in averages:
                averages[node_id] = {}
                averages[node_id]['V_Hi_vals'] = []
                averages[node_id]['I_Hi_vals'] = []
                averages[node_id]['V_Lo_vals'] = []
                averages[node_id]['I_Lo_vals'] = []
            function_id = int(_id) >> 8 & 0xFFFF

            if function_id == 0xAA00:  # Hi values
                averages[node_id]['V_Hi_vals'].append(struct.unpack('f', bytes.fromhex(_data[0:8]))[0])
                averages[node_id]['I_Hi_vals'].append(struct.unpack('f', bytes.fromhex(_data[8:16]))[0])
            if function_id == 0xAA01:  # Lo values
                averages[node_id]['V_Lo_vals'].append(struct.unpack('f', bytes.fromhex(_data[0:8]))[0])
                averages[node_id]['I_Lo_vals'].append(struct.unpack('f', bytes.fromhex(_data[8:16]))[0])
            else:
                pass
                # print("unknown function ID:{0:#0{1}x}".format(function_id, 4))
        # print(averages)
        for node_id, values in averages.items():
            print("aggregating {}".format(node_id))
            data_body = deepcopy(self.data_body_template)
            alias = node_id  # in case there are now infos, we use the ID as alias
            if node_id in self.config['node_infos']:  # if we have the alias, we use it
                alias = self.config['node_infos'][node_id]['alias']
            try:  # we may have 0 values
                averages[node_id]['V_Hi_ave'] = sum(values['V_Hi_vals']) / len(values['V_Hi_vals'])
                averages[node_id]['I_Hi_ave'] = sum(values['I_Hi_vals']) / len(values['I_Hi_vals'])
                averages[node_id]['V_Lo_ave'] = sum(values['V_Lo_vals']) / len(values['V_Lo_vals'])
                averages[node_id]['I_Lo_ave'] = sum(values['I_Lo_vals']) / len(values['I_Lo_vals'])
                data_body['tags']['node_id'] = node_id
                data_body['tags']['alias'] = alias
                data_body['fields']['V_Hi'] = averages[node_id]['V_Hi_ave']
                data_body['fields']['I_Hi'] = averages[node_id]['I_Hi_ave']
                data_body['fields']['V_Lo'] = averages[node_id]['V_Lo_ave']
                data_body['fields']['I_Lo'] = averages[node_id]['I_Lo_ave']
                # calculating power
                data_body['fields']['P_Hi'] = averages[node_id]['I_Hi_ave'] * averages[node_id]['V_Hi_ave']
                data_body['fields']['P_Lo'] = averages[node_id]['I_Lo_ave'] * averages[node_id]['V_Lo_ave']

                data_body['time'] = "{}".format(datetime.now())
                # print(data_body)
                self.upload_queue.put(deepcopy(data_body))
            except Exception as e:
                print(e)
        # print(averages)
        # _id = item['id']
        # _data = item['data'].hex()
        # temp_bytes = bytes.fromhex(_data[0:8])  # getting bytes from data
        # temp = int.from_bytes(temp_bytes, "little") / 100.0
        # RH_bytes = bytes.fromhex(_data[8:16])
        # RH = int.from_bytes(RH_bytes, "little") / 100.0
        # print("T:{}, RH:{}".format(temp,RH))

    def run(self):
        logging.info("aggregator starting")
        while self.running:
            time.sleep(self.config['time'])
            self.aggregate()
