import queue
import logging
from copy import deepcopy
import threading
import smbus2 as smbus
import time
from datetime import datetime

class I2CThread(threading.Thread):
    def __init__(self, config, queue, data_body):
        threading.Thread.__init__(self, name="i2cthread")
        self.running = True
        self.config = config
        self.sensors = config['sensors']
        self.i2c_bus_number = config['ic2_bus_number']
        self.node_id = config['node_id']
        self.alias = config['alias']
        self.data_body_template = data_body
        self.q = queue
        self.handle_imports()
        # self.thread = threading.Thread(target=self.i2c_thread)
    
    def handle_imports(self):
        for sensor in self.sensors:
            if sensor['type'] == "BME280":
                print("using bme280, importing library")
                import bme280

    def handle_BME280(self, sensor):
        import bme280
        address = sensor['address']

        calibration_params = bme280.load_calibration_params(self.bus, address)
        data = bme280.sample(self.bus, address, calibration_params)

        # the compensated_reading class has the following attributes
        print(data.id)
        print(data.timestamp)
        print(data.temperature)
        print(data.pressure)
        print(data.humidity)
        data_body = deepcopy(self.data_body_template)
        data_body['tags']['node_id'] = self.node_id
        data_body['tags']['alias'] = self.alias
        data_body['fields']['Temp'] = data.temperature
        data_body['fields']['Press'] = data.pressure
        data_body['fields']['RelHum'] = data.humidity
        data_body['time'] = "{}".format(datetime.now())
        # print(data_body)
        self.q.put(deepcopy(data_body))

    def get_sensors(self):
        print("getting sensors")
        for sensor in self.sensors:
            if sensor['type'] == "BME280":
                self.handle_BME280(sensor)

    def run(self):
        self.bus = smbus.SMBus(self.i2c_bus_number)
        logging.info("aggregator starting")
        while self.running:
            time.sleep(self.config['time'])
            self.get_sensors()
            # data = bus.read_i2c_block_data(self.address, 0x00, 32)

            # self.q.put(data)
            # time.sleep(1)
            # break

    # def start(self):
    #     self.thread.start()

    def stop(self):
        self.running = False
        # self.thread.join()

if __name__ == "__main__":
    config = {
        "ic2_bus_number": 2,
        "node_id": 0x1001,
        "alias": "test",
        "sensors":[
        {"address": 0x76,"type": "BME280"}]
    }    
    print(config)
    queue = queue.Queue()
    i2cthread = I2CThread(config, queue, {})
    i2cthread.start()
    i2cthread.stop()

# i2c_thread = I2CThread(0x76)
# i2c_thread.start()