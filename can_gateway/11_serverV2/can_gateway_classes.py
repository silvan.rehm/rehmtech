import datetime
import json
import logging
import socket
import sys
import threading
import time
from queue import Queue

import can
import struct


class Updater(threading.Thread):
    def __init__(self, db):
        self.db = db
        self.bus = can.Bus(interface="socketcan", channel="can1", receive_own_messages=False)

        # self.data_header = []
        # db['dev']['t_rec_next'] = datetime.datetime.now()
        # db['dev']['t_log_next'] = datetime.datetime.now()
        threading.Thread.__init__(self, name="UpdaterLogger")
        logging.info("UpdaterLogger initialized")

    def update_can(self):
        can_queue: Queue = self.db['can_send_queue']
        if not can_queue.empty():
            try:
                self.bus.send(can_queue.get(), timeout=2.0)
                can_queue.task_done()
            except Exception as e:
                logging.exception(e)

        msg = self.bus.recv(2.0)
        new_msg = {'timestamp': msg.timestamp, 'data': msg.data.hex()}
        self.db['can_msgs'][msg.arbitration_id] = new_msg
        self.db['can_recv_queue'].put(msg)

        # >> > msgs[msg.arbitration_id][timestamp] = msg.timestamp
        # msg.timestamp
        # >> > msgs[msg.arbitration_id]['timestamp'] = msg.timestamp
        # >> > msgs[msg.arbitration_id]['data'] = msg.data
        # >> > json.dumps(msgs)
        # '{"11141131": {"timestamp": 1591808918.53518, "data": "3b5630421bc5c7bf"}}'
        # >> > import datetime
        # >> > datetime.datetime.fromtimestamp(msg.timestamp)
        # datetime.datetime(2020, 6, 10, 18, 8, 38, 535180)
        # logging.info(msg)
        # data = msg.data  # byte array 0..8 bytes
        # logging.info(json.dumps(msg))
        # _id = msg.arbitration_id
        #

        """
        import struct   
        data = [0xD1, 0x22, 0xAB, 0x3F]
        struct.unpack('f', bytes(data))
        (1.3370000123977661,)
        
        started defining can node numbers, based on I2C Numbers:
        /home/silvan/Nextcloud/10_Rehm-Engineering/20_MCU_Software/CAN_Nodes.ods
        
        New: 15 functions per node to define.
        
        0. broadcast
        1. commands
        10. SBC-values, voltage & current
        11. SBC-values, internal
        
        Data Length: 8 Bytes (4 floats)
        
        perfect for V_In, I_In, V_out, I_out 
        
        --> sending 4 floats every second
        
        ```cpp
        char node_id = 0x0A;
        void send_analog_values(){
        char function_id = 10;  // 10 -> internal analog values
        int msg_id = function_id << 8 | node_id; // adding 10 in front of the node ID as message ID
        can.write(CANMessage(msg_id, p, 8, CANData, CANExtended));
            }
        """
        # self.db['data'] = data

    def run(self):
        db = self.db
        logging.info("UpdaterLogger started")

        while db['running']:
            try:
                self.update_can()
            except Exception as e:
                logging.exception(e)
                pass

        #     try:
        #         self.are_slaves_done_recording()  # closes zip file
        #         if db['record_next_waveform']:
        #             db['record_next_waveform'] = False
        #             self.record_waveform()
        #         if datetime.datetime.now() > db['dev']['t_rec_next']:  # record waveform
        #             self.record_waveform()
        #     except Exception as e:
        #         logging.exception(e)
        #     try:
        #         self.process_scopes()
        #         self.process_inserts()
        #         self.process_primary_loop()
        #         self.process_HCS()
        #     except Exception as e:
        #         logging.exception(e)
        #
        #     if db['make_new_log_file']:  # if we need a new logfile
        #         if 'log_file' in db['dev']:
        #             db['dev']['log_file'].close()
        #             db['dev'].pop('log_file', None)
        #         db['make_new_log_file'] = False
        #     try:
        #         self.update_cycles()
        #     except Exception as e:
        #         logging.exception(e)
        #     try:
        #         if datetime.datetime.now() > db['dev']['t_log_next']:
        #             db['dev']['t_log_next'] += datetime.timedelta(0, db['dt_logging'])
        #             self.do_logging()
        #     except Exception as e:
        #         logging.exception(e)
        #     try:
        #         if not self.check_limits():  # after logging so we have the bad value in the old one
        #             db['server_config']['password'] = "thisisfine"
        #             db['server']['controller'] = "nobody"
        #             aux_core.stop_stress(db, key="error")  # TODO: Tester Core to handle these things
        #     except Exception as e:
        #         logging.exception(e)
        #         aux_core.stop_stress(db, key="error")
        #     try:
        #         dt = (datetime.datetime.now() - db['dev']['t0']).total_seconds()
        #         dt_UI = db['dt_UI']
        #         rest = dt_UI * (dt//dt_UI + 1) - dt
        #         time.sleep(rest)
        #     except Exception as e:
        #         logging.exception(e)
        #
        # # if we are here, running is over
        # if 'log_file' in db['dev']:
        #     db['dev']['log_file'].close()
        logging.info("updaterlogger died")


class ClientThread(threading.Thread):
    def __init__(self, ip, port, _socket, db):
        self.ip = ip
        self.port = port
        self.socket = _socket
        self.db = db
        self.client = None
        logging.info("[+] New thread started for " + ip + ":" + str(port))
        db['server']['no_clients'] += 1
        # threading.Thread.__init__(self)
        threading.Thread.__init__(self, name="ClientServer_" + ip + ":" + str(port))

    def register_client(self, client, db):
        self.client = client
        logging.info("registering %s" % client)
        db['server']['clients'][client['UUID']] = client

    def send_db(self):
        db = self.db
        try:
            # db_no_dev = db.copy()
            # db_no_dev.pop('dev', None)
            # db_no_dev.pop('dfs', None)
            # db_no_dev = {"hihi": "rofl", 'time': "%i" % (time.time() * 1000.0), "data": db['data'].decode()}
            db_to_send = {"can_msgs": db['can_msgs'].copy()}
            self.socket.sendall(json.dumps(db_to_send).encode())
        except Exception as e:
            logging.exception("problem with encoding data!")
            logging.exception(e)
            logging.exception(db["data"])
            self.socket.sendall(json.dumps({}).encode())

    def deal_with_data(self, data_str, db):
        try:
            db['server']['recv'] = json.loads(data_str)
        except Exception as e:
            logging.exception(data_str)
            logging.exception(e)
            self.send_db()
            return
        if "enable_node" in db['server']['recv']:
            self.enable_node(db['server']['recv']['enable_node'])
        if "disable_node" in db['server']['recv']:
            self.disable_node(db['server']['recv']['disable_node'])
        if "set_mode" in db['server']['recv']:
            self.set_mode(db['server']['recv']['set_mode'])
        if "set_node_id" in db['server']['recv']:
            self.set_node_id(db['server']['recv']['set_node_id'])
        if "set_PWM_DC" in db['server']['recv']:
            self.set_PWM_DC(db['server']['recv']['set_PWM_DC'])
        if "set_PWM_freq" in db['server']['recv']:
            self.set_PWM_freq(db['server']['recv']['set_PWM_freq'])
        if "set_current" in db['server']['recv']:
            self.set_current(db['server']['recv']['set_current'])
        if "set_max_current" in db['server']['recv']:
            self.set_max_current(db['server']['recv']['set_max_current'])
        if "set_voltage" in db['server']['recv']:
            self.set_voltage(db['server']['recv']['set_voltage'])

        self.send_db()

    def run(self):
        db = self.db
        data = "dummydata"
        while len(data):
            data = self.socket.recv(65536)
            data_str = data.decode()
            if len(data_str) > 0:  # only when actual data was sent
                try:
                    self.deal_with_data(data_str, db)
                except Exception as e:
                    logging.exception(e)
                    break
        logging.info("my client disconnected")
        db['server']['no_clients'] -= 1
        if self.client:
            db['server']['clients'].pop(self.client['UUID'], None)
        self.socket.close()
        logging.info("ClientServer_" + self.ip + ":" + str(self.port) + " died")

    def enable_node(self, node_id):
        logging.info("enabling node %s" % hex(node_id))
        cmd_id = 0x0000
        data = [0xFF]  # enable
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

    def disable_node(self, node_id):
        logging.info("disable_node %s" % hex(node_id))
        cmd_id = 0x0000
        data = [0x00]  # disable
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

    def set_mode(self, param):
        node_id = param[0]
        mode_id = param[1]
        logging.info("set_mode %s to %s" % (hex(node_id), hex(mode_id)))
        cmd_id = 0x0010
        data = [mode_id]
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

    def set_node_id(self, param):
        node_id = param[0]
        new_node_id = param[1]
        logging.info("set_node_id %s to %s" % (hex(node_id), hex(new_node_id)))
        cmd_id = 0x0020
        data = [0x40, new_node_id]
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

    def set_PWM_DC(self, param):
        # _dict = {"set_PWM_DC": [_target, _DC]}
        node_id = param[0]
        _DC = param[1]
        _DC_int = int(_DC * 0xFFFF)
        _DC_byte0 = _DC_int & 0xFF
        _DC_byte1 = _DC_int >> 8 & 0xFF
        logging.info("set_PWM_DC %s to %s" % (hex(node_id), hex(_DC_int)))

        cmd_id = 0x0020
        data = [0x10, _DC_byte1, _DC_byte0]
        logging.info(" ".join(["%s" % hex(_d) for _d in data]))
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

    def set_PWM_freq(self, param):
        node_id = param[0]
        _f = param[1]
        _f_int = int(_f / 4.0)
        _f_byte0 = _f_int & 0xFF
        _f_byte1 = _f_int >> 8 & 0xFF
        # _freq_bytes = list(struct.pack('f', param[1]))

        # _DC_int = int(_freq * 0xFFFF)
        # _DC_byte0 = _DC_int & 0xFF
        # _DC_byte1 = _DC_int >> 8 & 0xFF
        logging.info("set_PWM_freq %s to %f" % (hex(node_id), _f))

        cmd_id = 0x0020
        data = [0x11, _f_byte1, _f_byte0]
        logging.info(" ".join(["%s" % hex(_d) for _d in data]))
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

    def set_current(self, param):        
        node_id = param[0]
        _current = param[1]
        logging.info("setting current to %.2f" % _current)
        _current_int = int(_current * 1000.0 + 0x7FFF)
        _DC_byte0 = _current_int & 0xFF
        _DC_byte1 = _current_int >> 8 & 0xFF
        logging.info("set_current %s to %s" % (hex(node_id), hex(_current_int)))

        cmd_id = 0x0020
        data = [0x20, _DC_byte1, _DC_byte0]
        logging.info(" ".join(["%s" % hex(_d) for _d in data]))
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)
    
    def set_max_current(self, param):        
        node_id = param[0]
        _current = param[1]
        logging.info("setting max current to %.2f" % _current)
        _current_int = int(_current * 1000.0 + 0x7FFF)
        _DC_byte0 = _current_int & 0xFF
        _DC_byte1 = _current_int >> 8 & 0xFF
        logging.info("set_max_current %s to %s" % (hex(node_id), hex(_current_int)))

        cmd_id = 0x0020  # set value
        data = [0x21, _DC_byte1, _DC_byte0]
        logging.info(" ".join(["%s" % hex(_d) for _d in data]))
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

    def set_voltage(self, param):        
        node_id = param[0]
        volt_f = param[1]
        logging.info("setting voltage to %.2f" % volt_f)
        _current_int = int(volt_f * 1000.0)
        _byte0 = _current_int & 0xFF
        _byte1 = _current_int >> 8 & 0xFF
        logging.info("set_voltage %s to %s" % (hex(node_id), hex(_current_int)))

        cmd_id = 0x0020
        data = [0x30, _byte1, _byte0]
        logging.info(" ".join(["%s" % hex(_d) for _d in data]))
        msg_id = cmd_id << 8 | int(node_id)
        msg = can.Message(arbitration_id=msg_id, is_extended_id=True, data=data)
        self.db['can_send_queue'].put(msg)

class ClientHandler(threading.Thread):
    def __init__(self, db):
        self.db = db
        threading.Thread.__init__(self, name="ClientHandler")
        db['dev']['socket'] = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            HOST = db['server_config']['host']
            PORT = db['server_config']['port']
            db['dev']['socket'].bind((HOST, PORT))
        except socket.error as msg:
            logging.exception(msg)
            sys.exit()

    def run(self):
        db = self.db
        while db['running']:
            db['dev']['socket'].listen(4)
            logging.info('Socket now listening')
            (clientsock, (ip, port)) = db['dev']['socket'].accept()
            newthread = ClientThread(ip, port, clientsock, db)
            newthread.setDaemon(True)
            newthread.start()
        logging.info("clienthandler died")
