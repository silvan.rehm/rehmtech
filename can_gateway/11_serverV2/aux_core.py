import logging
import os
import socket
import sys
import threading
import time
import shutil

import yaml


def load_config():
    db = yaml.safe_load(open("config/init_can_gateway_server.yaml"))
    local_config_dir = os.path.expanduser(db['local_config_dir'])
    if not os.path.exists(local_config_dir):
        os.makedirs(local_config_dir)
    local_fn = os.path.join(local_config_dir, db['local_config_name'])
    if os.path.isfile(local_fn):  # if we have a local file, we load it
        local_db = yaml.safe_load(open(local_fn))
    else:  # if we don't have a local file, we copy the template and load it
        shutil.copyfile(os.path.join("config",db['local_config_name']), local_fn)
        local_db = yaml.safe_load(open(local_fn))
    if not local_db['values_updated']:
        print("update {} first!".format(local_fn))
        sys.exit(0)
    db.update(local_db)
    return db

def prepare_logging(db):
    log_dir = os.path.expanduser(db['log_dir'])

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)s] [%(levelname)s]  %(message)s")
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.NOTSET)

    file_handler = logging.FileHandler(os.path.join(log_dir, "%i.txt" % (time.time() * 1000.0)))
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    log = logging.getLogger('pymodbus')
    log.setLevel(logging.ERROR)
    log = logging.getLogger('can')
    log.setLevel(logging.ERROR)
    log = logging.getLogger('matplotlib')
    log.setLevel(logging.ERROR)


def shut_down(db):
    logging.info("shutting down")
    db['running'] = False
    HOST = db['server_config']['host']
    PORT = db['server_config']['port']
    # connecting to self to close
    _socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    _socket.connect((HOST, PORT))
    time.sleep(1.1)
    _socket.close()
    logging.info("still alive threads:")
    for thread in threading.enumerate():
        logging.info(thread)
    sys.exit(0)
