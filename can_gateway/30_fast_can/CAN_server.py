import socket
import threading
import struct
from can import Bus
import time
import json

BUFFER_SIZE = 1000
MAX_CLIENTS = 5

class CANServer:
    def __init__(self):
        self.buffer = []
        self.buffer_lock = threading.Lock()
        self.bus = Bus(interface='socketcan', channel='can0')
        self.clients = []

    def receive_can_messages(self):
        while True:
            msg = self.bus.recv(2.0)
            if msg:
                timestamp = time.time()
                _id = msg.arbitration_id
                _data = msg.data.hex()
                _node_id = int(_id) & 0xFF
                node_id = "{0:#0{1}x}".format(_node_id, 4)
                function_id = int(_id) >> 8 & 0xFFFF
                # Your processing logic here...
                processed_data = {
                    'timestamp': timestamp,
                    'node_id': node_id,
                    'function_id': function_id,
                    # Add more fields as needed...
                }
                with self.buffer_lock:
                    self.buffer.append(processed_data)
                    if len(self.buffer) > BUFFER_SIZE:
                        self.buffer.pop(0)

    def client_handler(self, conn):
        while True:
            with self.buffer_lock:
                buffer_copy = self.buffer.copy()
            conn.sendall(json.dumps(buffer_copy).encode())
            time.sleep(0.1) # Adjust this sleep time based on your needs

    def start(self, host='', port=65432):
        receiver_thread = threading.Thread(target=self.receive_can_messages)
        receiver_thread.start()

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((host, port))
            s.listen()
            while len(self.clients) < MAX_CLIENTS:
                conn, addr = s.accept()
                print('Connected by', addr)
                client_thread = threading.Thread(target=self.client_handler, args=(conn,))
                client_thread.start()
                self.clients.append(conn)

if __name__ == '__main__':
    server = CANServer()
    server.start()
