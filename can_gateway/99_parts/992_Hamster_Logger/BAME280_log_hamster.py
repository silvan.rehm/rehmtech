import datetime
import os
import time

import can

bus = can.Bus(interface="socketcan", channel="can0", receive_own_messages=False)

csv_fn = "%i_BME280.csv" % time.time()
csv_file = open(csv_fn, 'w', 1)  #  1 means line buffered
csv_file.write('time,T,RH')
csv_file.write(os.linesep)

while True:
    time.sleep(10)
    try:
        msg = bus.recv(2.0)
        data = msg.data.hex()
    except Exception as e:
        print(e)
        continue
    # print(data)
    temp_bytes = bytes.fromhex(data[0:8])
    temp = int.from_bytes(temp_bytes, "little") / 100.0
    # print(temp)
    RH_bytes = bytes.fromhex(data[8:16])
    RH = int.from_bytes(RH_bytes, "little") / 100.0
    print("{} °C / {} % RH".format(temp, RH))
    csv_file.write('%s,%.2f,%.2f' % (datetime.datetime.now(), temp, RH))
    csv_file.write(os.linesep)


