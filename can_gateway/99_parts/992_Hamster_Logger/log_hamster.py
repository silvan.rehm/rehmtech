import datetime
import os
import time

import can

bus = can.Bus(interface="socketcan", channel="can0", receive_own_messages=False)

csv_fn = "%i.csv" % time.time()
csv_file = open(csv_fn, 'w', 1)  # 1 means line buffered
csv_file.write('time,T,RH')
csv_file.write(os.linesep)

while True:
    msg = bus.recv(2.0)
    data = msg.data.hex()
    # print(data)
    temp_bytes = bytes.fromhex(data[0:8])
    temp = int.from_bytes(temp_bytes, "little")
    # print(temp)
    RH_bytes = bytes.fromhex(data[8:16])
    RH = int.from_bytes(RH_bytes, "little")
    print("{} °C / {} % RH".format(temp, RH))
    csv_file.write('%s,%i,%i' % (datetime.datetime.now(), temp, RH))
    csv_file.write(os.linesep)
