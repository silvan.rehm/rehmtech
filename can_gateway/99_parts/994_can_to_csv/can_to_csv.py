import can
import struct
import time
import os 

bus = can.Bus(interface="socketcan", channel="can0", receive_own_messages=False)
target_node = 0xFF;
V_Hi = 0
V_Lo = 0
I_Hi = 0
I_Lo = 0
log_file = open (f"{int(time.time())}.csv", 'w', 1)
header = ["VHi", "IHi","VLo","ILo"]
log_file.write(",".join(header) + os.linesep)
new_hi = False
new_lo = False
while True:
    msg = bus.recv(1.0)
    # new_msg = {'timestamp': msg.timestamp, 'data': msg.data.hex()}
    _id = msg.arbitration_id
    _data = msg.data.hex()
    _node_id = int(_id) & 0xFF
    function_id = int(_id) >> 8 & 0xFFFF

    if _node_id != target_node:
        print("woti ned")
    else:       

        if function_id == 0xAA00:  # Hi values
            V_Hi = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            I_Hi = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            new_hi = True
        if function_id == 0xAA01:  # Lo values
            V_Lo = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            I_Lo = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            new_lo = True
    if new_lo and new_hi:
        log_file.write(",".join([f"{V_Hi:.5f}", f"{I_Hi:.5f}", f"{V_Lo:.5f}", f"{I_Lo:.5f}"]) + os.linesep)
        new_hi = False
        new_lo = False
    #print(f"Hi: {V_Hi:.3f} V / {I_Hi:.3f} A // Lo: {V_Lo:.3f} V / {I_Lo:.3f} A")
