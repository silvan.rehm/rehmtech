import sys

from influxdb import InfluxDBClient

# grafana login data
# client = InfluxDBClient(host="192.168.1.244", port=8086, username='admin', password='Welcome1')
"""Instantiate a connection to the InfluxDB."""
host = "192.168.1.244"
port = 8086
admin_user = 'admin'
admin_password = 'Welcome1'
dbname = 'example'
dbuser = 'smly'
dbuser_password = 'my_secret_password'
query = 'select Float_value from cpu_load_short;'
query_where = 'select Int_value from cpu_load_short where host=$host;'
bind_params = {'host': 'server01'}
json_body = [
    {
        "measurement": "cpu_load_short",
        "tags": {
            "host": "server01",
            "region": "us-west"
        },
        "time": "2020-07-16T23:40:00Z",
        "fields": {
            "Float_value": 5.04,
            "Int_value": 3,
            "String_value": "Lol",
            "Bool_value": True
        }
    }
]

# client = InfluxDBClient(host, port, admin_user, admin_password, dbname)
#
# print("Create database: " + dbname)
# client.create_database(dbname)
#
# sys.exit(0)

# print("Create a retention policy")
# client.create_retention_policy('example', '3d', 3, default=True)

# print("Switch user: " + dbuser)
# client.switch_user(dbuser, dbuser_password)

client = InfluxDBClient(host, port, dbuser, dbuser_password, dbname)

print("Write points: {0}".format(json_body))
client.write_points(json_body)

print("Querying data: " + query)
result = client.query(query)

print("Result: {0}".format(result))

print("Querying data: " + query_where)
result = client.query(query_where, bind_params=bind_params)

print("Result: {0}".format(result))

# print("Switch user: " + admin_user)
# client.switch_user(admin_user, admin_password)

# print("Drop database: " + dbname)
# client.drop_database(dbname)