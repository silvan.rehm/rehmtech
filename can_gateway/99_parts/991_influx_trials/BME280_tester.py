from datetime import datetime
import json
import os
import sys
import time
#from influxdb import InfluxDBClient

import can


# DB Config
host = "192.168.1.244"
port = 8086
dbname = 'BME280_trial'
dbuser = 'BME280'
dbuser_password = 'pff'

admin_user = 'admin'
admin_password = 'Welcome1'

json_body = [
    {
        "measurement": "ambient_values",
        "tags": {
            "position": "gamer-room",
            "floor": "2",
            "building": "house"
        },
        "time": "2020-07-16T23:40:00Z",
        "fields": {
            "Temperature": 25.0,
            "Humidity": 50.0,
            "Pressure": 0.0
        }
    }
]

# client = InfluxDBClient(host, port, admin_user, admin_password, dbname)
#
# print("Create database: " + dbname)
# client.create_database(dbname)
#
# sys.exit(0)



# print("{0}".format(json_body))

#client = InfluxDBClient(host, port, dbuser, dbuser_password, dbname)

# print("Write points: {0}".format(json_body))
# client.write_points(json_body)

# sys.exit(0)


bus = can.Bus(interface="socketcan", channel="can0", receive_own_messages=False)
#
# csv_fn = "%i_BME280.csv" % time.time()
# csv_file = open(csv_fn, 'w', 1)  #  1 means line buffered
# csv_file.write('time,T,RH')
# csv_file.write(os.linesep)


while True:
    msg = bus.recv(2.0)
    data = msg.data.hex()
    # print(data)
    temp_bytes = bytes.fromhex(data[0:8])
    temp = int.from_bytes(temp_bytes, "little") / 100.0
    # print(temp)
    RH_bytes = bytes.fromhex(data[8:16])
    RH = int.from_bytes(RH_bytes, "little") / 100.0

    print("{} °C / {} % RH".format(temp, RH))

    json_body[0]['time'] = "{}".format(datetime.now())
    json_body[0]['fields']['Temperature'] = temp
    json_body[0]['fields']['Humidity'] = RH
    #print("Write points: {0}".format(json_body))
    #client.write_points(json_body)
    # csv_file.write('%s,%.2f,%.2f' % (datetime.datetime.now(), temp, RH))
    # csv_file.write(os.linesep)


