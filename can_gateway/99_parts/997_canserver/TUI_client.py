import curses
import logging
from client_functions import receive_data, ask_for_value, generate_and_send_CAN_msg
from DataRecorder import DataRecorder
import client_functions as cf
import socket
import struct
import pickle
import numpy as np
import matplotlib.pyplot as plt
import time
import threading
from collections import defaultdict
from typing import List, Dict
from can import Message

command_actions = {
    'S': {
        'prompt': "Select Node",
        'range': [0, 255],
        'config_key': "target_node"
    },
    'D': {
        'prompt': "Target Duty Cycle:",
        'range': [1, 99],
        'config_key': "set_duty_cycle",
        'send_msg': True
    },
    'F': {
        'prompt': "PWM frequency in kHz:",
        'range': [1, 250],
        'config_key': "set_PWM_freq",
        'send_msg': True
    },
    # Add other commands as needed...
}

def tui(screen, new_data_event, config, client_socket, command_actions):
    # Curses initialization
    curses.curs_set(0)  # Hide the cursor
    screen.nodelay(1)  # Non-blocking mode, screen.getch() won't wait for input
    curses.start_color()
    recorder = DataRecorder(data_storage)

    def display_data():
        screen.addstr("ID\tV_Hi\tI_Hi\tV_Lo\tI_Lo\tm\tmode\n")
        
        line_no = 3
        for node_id, data in data_storage.items():
            if len(data["V_Hi"]) < 10:  # Ensure there are at least 10 data points
                continue

            # Compute medians over the last N values
            v_hi_median = np.median(data["V_Hi"][-10:])
            i_hi_median = np.median(data["I_Hi"][-10:])
            v_lo_median = np.median(data["V_Lo"][-10:])
            i_lo_median = np.median(data["I_Lo"][-10:])

            # Assuming m and mode are stored in data, e.g., data["m"] and data["mode"]
            m_value = data["m"][-1] if "m" in data else 0
            mode_value = data["mode"][-1] if "mode" in data else "N/A"

            line = "{:<4}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.0f}\t{}".format(
                node_id, v_hi_median, i_hi_median, v_lo_median, i_lo_median, m_value, mode_value
            )

            screen.addstr(line +"\n")
            line_no += 1

    # Define a method to print the menu
    def print_menu(sub_menu):        
        menu = "[S]elect node, Target [C]urrent, [V]oltage, [M]ode, [D]utycycle, [F]req, show [P]lot, [R]ecord Data"
        screen.addstr(menu+"\n")
        screen.addstr(str(config) +"\n")
        screen.addstr(sub_menu+"\n")
        screen.addstr(str(time.time())+"\n")

    # Main loop for the TUI
    sub_menu = "-"
    while True:
        # screen.clear()        
        screen.erase()        
        display_data()
        print_menu(sub_menu)
        
        screen.refresh()

        screen.nodelay(1)  # Enable non-blocking mode
        ch = screen.getch()

        # Check if the character is in the dictionary (case-insensitive)
        action = command_actions.get(chr(ch).upper(), None) if 0 <= ch < 0x110000 else None
        try:
            if action:
                answer = ask_for_value(screen, action['prompt'], action['range'])
                if answer:
                    config[action['config_key']] = answer
                    if action.get('send_msg', False):  # Only send a message if the flag is set
                        generate_and_send_CAN_msg(action['config_key'], config, client_socket)
            elif ch in (ord('R'), ord('r')):
                if recorder.recording:
                    recorder.stop_recording()
                else:
                    recorder.start_recording()
            elif ch in (ord('Q'), ord('q')):
                break  # Exit loop on pressing 'q'
        except Exception as e:
            logging.exception(e)
        new_data_event.wait(0.5)


cf.prepare_logging("~/python/logs/CAN_TUI")


# Connect to the BeagleBone server
server_address = ('192.168.1.221', 12345)  # Replace with the BeagleBone's IP address
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(server_address)
logging.info("Connected to server!")

# data storage with a limit of 100k elements
data_storage: Dict[str, Dict[str, np.array]] = defaultdict(lambda: defaultdict(lambda: np.array([], dtype=float)))

# Create an event for notifying new data
new_data_event = threading.Event()

# Temporary storage dictionary
temp_storage: Dict[str, float] = {}

plot_duration = 15  # Display last 60 seconds (1 minute) of data
N_max = 1000

# Start the data receiving thread
data_thread = threading.Thread(target=receive_data, 
        args = (client_socket, temp_storage, data_storage, new_data_event,), daemon=True)
data_thread.start()

config = {
    "target_node": 0
}

curses.wrapper(lambda screen: tui(screen, new_data_event, config, client_socket, command_actions))
