import socket
import struct
import pickle
import numpy as np
import matplotlib.pyplot as plt
import time
import threading
from collections import defaultdict
from typing import List, Dict
from can import Message

# data storage with a limit of 100k elements
data_storage: Dict[str, Dict[str, np.array]] = defaultdict(lambda: defaultdict(lambda: np.array([], dtype=float)))

# Create an event for notifying new data
new_data_event = threading.Event()

# Temporary storage dictionary
temp_storage: Dict[str, float] = {}

plot_duration = 15  # Display last 60 seconds (1 minute) of data
N_max = 1000


# Connect to the BeagleBone server
server_address = ('192.168.1.221', 12345)  # Replace with the BeagleBone's IP address
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(server_address)
print("Connected to server!")

def send_can_messages():
    while True:
        # Create a CAN message with the desired properties.
        # This is a basic example, and you may need to adjust it to fit your needs.
        msg = Message(arbitration_id=0x123, data=[0, 1, 2, 3, 4, 5, 6, 7])
        
        # Serialize and send the CAN message
        serialized_msg = pickle.dumps(msg)
        # client_socket.sendall(len(serialized_msg).to_bytes(4, 'big'))
        client_socket.sendall(serialized_msg)
        
        print("Message sent!")
        
        # Wait for one second
        time.sleep(1)

def check_and_store(node_id: str) -> None:
    if 'time' not in temp_storage:
        return
    # Check if temp_storage contains all required fields and timestamp difference is not too large
    if set(temp_storage.keys()) == {'time', 'V_Hi', 'I_Hi', 'V_Lo', 'I_Lo'} and abs(temp_storage['time'] - temp_storage['V_Hi'][1]) <= 350e-6:
        for key in ['V_Hi', 'I_Hi', 'V_Lo', 'I_Lo']:
            data_storage[node_id][key] = np.append(data_storage[node_id][key][-N_max:], temp_storage[key][0])  # append value and keep the last 100000 values only
        data_storage[node_id]['timestamps'] = np.append(data_storage[node_id]['timestamps'][-N_max:], temp_storage['time'])
        temp_storage.clear()
    else:
        # Debug print statements:
        if time not in temp_storage:
            print(f"time not in temp_storage: {time}")        
            return        
        print(f"Keys in temp_storage: {set(temp_storage.keys())}")
        print(f"Timestamp difference: {abs(temp_storage['time'] - temp_storage['V_Hi'][1])}")
        print(f"keys not complete: {set(temp_storage.keys())}")


# Function to process received messages
def process_messages(messages: List[Message]) -> None:    
    if not messages:
        return
    for message in messages:        
        _id = message.arbitration_id
        _node_id = int(_id) & 0xFF
        node_id = "{0:#0{1}x}".format(_node_id, 4)  # into nicelooking strings (i.e. 0x0A)  
        function_id = int(_id) >> 8 & 0xFFFF
        _data = message.data.hex()

        if function_id == 0xAA00:  # Hi values
            V_Hi = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            I_Hi = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            temp_storage["V_Hi"] = (V_Hi, message.timestamp)
            temp_storage["I_Hi"] = (I_Hi, message.timestamp)
        elif function_id == 0xAA01:  # Lo values
            V_Lo = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            I_Lo = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            temp_storage["V_Lo"] = (V_Lo, message.timestamp)
            temp_storage["I_Lo"] = (I_Lo, message.timestamp)
            check_and_store(node_id)  # Check if we can store the values
        elif function_id == 0xaa80:
            
            temp_storage.clear()  # Clear temp_storage
            temp_storage['time'] = message.timestamp
    new_data_event.set()  # Signal that new data is available

# Function to receive data from the server
def receive_data() -> None:
    try:
        while True:
            # First receive 4 bytes for the length
            length_data = client_socket.recv(4)
            length = int.from_bytes(length_data, 'big')

            # Then receive the data according to the length
            data = b''
            while len(data) < length:
                chunk = client_socket.recv(4096)
                data += chunk

            # Deserialize the received data
            serialized_messages = pickle.loads(data)
            messages = [pickle.loads(message) for message in serialized_messages]

            # Process the received CAN messages
            process_messages(messages)

    except KeyboardInterrupt:
        print("Disconnected from server.")
    finally:
        client_socket.close()

# Function to plot data
def plot_data() -> None:
    plt.ion()  # Turn on interactive mode
    fig, axs = plt.subplots(4, 1, sharex=True)
    fig.suptitle("Data")

    while True:
        new_data_event.wait()  # Wait until new data is available
        try:
            for node_id, data in data_storage.items():
                for ax in axs:
                    ax.clear()  # Clear previous plots

                time_aligned = data["timestamps"] - data["timestamps"][0]

                axs[0].plot(time_aligned, data["V_Hi"])
                axs[0].set(ylabel="V_Hi")

                axs[1].plot(time_aligned, data["I_Hi"])
                axs[1].set(ylabel="I_Hi")

                axs[2].plot(time_aligned, data["V_Lo"])
                axs[2].set(ylabel="V_Lo")

                axs[3].plot(time_aligned, data["I_Lo"])
                axs[3].set(xlabel="Time (s)", ylabel="I_Lo")

            fig.canvas.draw()
            plt.pause(0.01)  # Allow time for update
        except Exception as e:
            print(e)
        new_data_event.clear()  # Clear the event


# Start the data receiving thread
data_thread = threading.Thread(target=receive_data)
data_thread.start()

# After defining the send_can_messages function:
send_thread = threading.Thread(target=send_can_messages)
send_thread.start()


# Start plotting data
plot_data()
