import socket
import struct
import pickle
from typing import List
from can import Message

# Connect to the BeagleBone server
server_address = ('192.168.1.221', 12345)  # Replace with the BeagleBone's IP address
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(server_address)
print("Connected to server!")

# Function to process received messages
def process_messages(messages: List[Message]) -> None:
    if not messages:
        return
    num_messages = len(messages)
    time_span = messages[-1].timestamp - messages[0].timestamp
    print("")
    print(f"Received {num_messages} messages which cover {time_span:.2f} seconds")
    
    for message in messages[-10:]:        
        _id = message.arbitration_id
        _data = message.data.hex()
        _node_id = int(_id) & 0xFF
        node_id = "{0:#0{1}x}".format(_node_id, 4)  # into nicelooking strings (i.e. 0x0A)  
        function_id = int(_id) >> 8 & 0xFFFF
        # print(str(message))
        # print(f"first message from {node_id}, function: {function_id}, data: {_data}")
        if function_id == 0xAA00:  # Hi values
            V_Hi = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            I_Hi = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            print(f"{node_id} - V_Hi:{V_Hi}, I_Hi:{I_Hi}")
        if function_id == 0xAA01:  # Lo values
            V_Lo = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            I_Lo = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            print(f"{node_id} - V_Lo:{V_Lo}, I_Lo:{I_Lo}")
        if function_id == 0xaa80:
            time_stamp = message.timestamp
            print(f"new time_stamp:{time_stamp}")
        else:
            print(f"unkown function_id: {function_id}")


try:
    # Continuously receive data from the server
    while True:
        # First receive 4 bytes for the length
        length_data = client_socket.recv(4)
        length = int.from_bytes(length_data, 'big')

        # Then receive the data according to the length
        data = b''
        while len(data) < length:
            chunk = client_socket.recv(4096)
            data += chunk

        # Deserialize the received data
        serialized_messages = pickle.loads(data)
        messages = [pickle.loads(message) for message in serialized_messages]

        # Process the received CAN messages
        process_messages(messages)
except KeyboardInterrupt:
    print("Disconnected from server.")
finally:
    client_socket.close()
