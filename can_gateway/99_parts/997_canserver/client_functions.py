import socket
import os
import logging
import struct
import pickle
import numpy as np
import matplotlib.pyplot as plt
import time
import threading
from collections import defaultdict
from typing import List, Dict
from can import Message
import curses
from CANMessageGenerator import CANMessageGenerator

def check_and_store(node_id: str, temp_storage, data_storage, N_max=1000) -> None:
    if 'time' not in temp_storage:
        return
    # Check if temp_storage contains all required fields and timestamp difference is not too large
    if set(temp_storage.keys()) == {'time', 'V_Hi', 'I_Hi', 'V_Lo', 'I_Lo', 'mode'} and abs(temp_storage['time'] - temp_storage['V_Hi'][1]) <= 350e-6:
        for key in ['V_Hi', 'I_Hi', 'V_Lo', 'I_Lo', 'mode']:
            data_storage[node_id][key] = np.append(data_storage[node_id][key][-N_max:], temp_storage[key][0])  # append value and keep the last 100000 values only
        data_storage[node_id]['timestamps'] = np.append(data_storage[node_id]['timestamps'][-N_max:], temp_storage['time'])
        temp_storage.clear()
    else:
        # Debug print statements:
        if 'time' not in temp_storage:
            print(f"time not in temp_storage: {temp_storage}")        
            return        
        print(f"Keys in temp_storage: {set(temp_storage.keys())}")
        print(f"Timestamp difference: {abs(temp_storage['time'] - temp_storage['V_Hi'][1])}")
        print(f"keys not complete: {set(temp_storage.keys())}")

def send_can_message(msg:Message, client_socket):       
    # Serialize and send the CAN message
    serialized_msg = pickle.dumps(msg)
    client_socket.sendall(serialized_msg)


def generate_and_send_CAN_msg(cmd, config, client_socket):
    # msg = generate_CAN_msg(cmd, config)    
    
    generator = CANMessageGenerator()
    msg = generator.generate_msg(cmd, config)
    send_can_message(msg, client_socket)
   

# Function to process received messages
def process_messages(messages: List[Message], temp_storage, data_storage, new_data_event) -> None:    
    if not messages:
        return
    for message in messages:        
        _id = message.arbitration_id
        _node_id = int(_id) & 0xFF
        node_id = "{0:#0{1}x}".format(_node_id, 4)  # into nicelooking strings (i.e. 0x0A)  
        function_id = int(_id) >> 8 & 0xFFFF
        _data = message.data.hex()
        can_data = message.data
        if function_id == 0xAA00:  # Hi values
            V_Hi = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            # >>> struct.unpack('ff',data)
            # (0.8524499535560608, 0.018720000982284546)

            I_Hi = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            temp_storage["V_Hi"] = (V_Hi, message.timestamp)
            temp_storage["I_Hi"] = (I_Hi, message.timestamp)
        elif function_id == 0xAA01:  # Lo values
            V_Lo = struct.unpack('f', bytes.fromhex(_data[0:8]))[0]
            I_Lo = struct.unpack('f', bytes.fromhex(_data[8:16]))[0]
            temp_storage["V_Lo"] = (V_Lo, message.timestamp)
            temp_storage["I_Lo"] = (I_Lo, message.timestamp)
            check_and_store(node_id, temp_storage, data_storage)  # Check if we can store the values
        elif function_id == 0xAA80:            
            temp_storage.clear()  # Clear temp_storage
            pwmCCR2Value, pwmARRValue, state = struct.unpack('>HHBxxx', can_data)
            temp_storage['mode'] = (pwmCCR2Value, message.timestamp)
            temp_storage['time'] = message.timestamp
    new_data_event.set()  # Signal that new data is available

# Function to receive data from the server
def receive_data(client_socket, temp_storage, data_storage, new_data_event) -> None:
    try:
        while True:
            # First receive 4 bytes for the length
            length_data = client_socket.recv(4)
            length = int.from_bytes(length_data, 'big')

            # Then receive the data according to the length
            data = b''
            while len(data) < length:
                chunk = client_socket.recv(4096)
                data += chunk

            # Deserialize the received data
            serialized_messages = pickle.loads(data)
            messages = [pickle.loads(message) for message in serialized_messages]

            # Process the received CAN messages
            try:
                process_messages(messages, temp_storage, data_storage, new_data_event)
            except Exception as e:
                logging.exception(e)
                break
    except KeyboardInterrupt:
        print("Disconnected from server.")
    finally:
        client_socket.close()


# Define methods for each menu item
def ask_for_value(screen, prompt_text, value_range):
    screen.addstr(prompt_text + f" ({value_range[0]}-{value_range[1]}): ")
    screen.clrtoeol()
    screen.nodelay(0)
    curses.echo()  # Enables the echoing of characters
    value_str = screen.getstr().decode('utf-8')  # Get input from user
    curses.noecho()  # Disables the echoing of characters
    
    # Check for escape or enter key without input
    if not value_str or value_str == chr(27):  # 27 is the escape key in ASCII
        return None

    try:
        # Check if user input is in hex format
        if value_str.startswith("0x") or value_str.startswith("0X"):
            value = int(value_str, 16)
        else:  # Otherwise, try interpreting it as a decimal number
            value = int(value_str)

        # Validate the value
        if value_range[0] <= value <= value_range[1]:
            screen.addstr("\nSelected successfully. Press any key to continue...")
            return value
        else:
            screen.addstr("\nValue out of range. Press any key to continue...")
            return None

    except ValueError:
        screen.addstr("\nInvalid input. Press any key to continue...")
        return None

def prepare_logging(log_dir):
    log_dir = os.path.expanduser(log_dir)

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)s] [%(levelname)s]  %(message)s")
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.NOTSET)

    file_handler = logging.FileHandler(os.path.join(log_dir, "%i.txt" % (time.time() * 1000.0)))
    file_handler.setFormatter(log_formatter)
    file_handler.setLevel(logging.INFO)
    root_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    console_handler.setLevel(logging.INFO)
    root_logger.addHandler(console_handler)
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('pymodbus')
    log.setLevel(logging.ERROR)
    log = logging.getLogger('matplotlib')
    log.setLevel(logging.ERROR)