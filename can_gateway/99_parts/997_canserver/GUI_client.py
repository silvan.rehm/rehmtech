import tkinter as tk
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
import threading
import time


class PlottingApp(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.geometry("1680x1024")
        self.title("Plotting App")

        # Initialize frames
        self.frame_plot = FramePlot(self)
        self.frame_config = FrameConfig(self)
        self.frame_controls = FrameControls(self)

        # Start data thread
        threading.Thread(target=self.data_thread, daemon=True).start()

    def data_thread(self):
        # Just to simulate data generation
        while True:
            data = np.random.randn(100)  # 100 random numbers
            self.frame_plot.plot(data)  # Update the plot
            time.sleep(1)  # Pause for a second


class FramePlot(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master, width=1120, height=819)
        self.pack(side="left", fill="both", expand=True)
        self.figure = Figure(figsize=(10, 5), dpi=100)
        self.ax = self.figure.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.figure, self)
        self.canvas.get_tk_widget().pack(side="top", fill="both", expand=True)

    def plot(self, data):
        self.ax.clear()
        self.ax.plot(data)
        self.canvas.draw()


class FrameConfig(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master, width=1120, height=205)
        self.pack(side="left", fill="both", expand=True)


class FrameControls(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master, width=560, height=1024)
        self.pack(side="right", fill="both", expand=True)


if __name__ == "__main__":
    app = PlottingApp()
    app.mainloop()
