import socket
import threading
from collections import deque
from can import Bus, Message
import time
import pickle

# Buffer for CAN messages
raw_message_buffer = deque(maxlen=6000)
new_messages_count = 0
client_new_messages = {}  # Dictionary to track new messages for each client

# Collection function to read messages from CAN and add to buffer
def collection_thread():
    global new_messages_count
    can_bus = Bus(interface="socketcan", channel="can1", receive_own_messages=False)  # Adjust as per your setup
    while True:
        message = can_bus.recv()
        raw_message_buffer.append(message)
        # Increment the new messages count for all clients
        for client in client_new_messages.keys():
            client_new_messages[client] += 1

# Server function to send new messages to client
def server_thread():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('0.0.0.0', 12345))
    server_socket.listen(1)
    print("Waiting for client...")
    client_socket, addr = server_socket.accept()
    print(f"Client {addr} connected!")

    # Initialize new messages counter for the new client
    client_new_messages[addr] = 0
    client_socket.setblocking(0)
    can_bus = Bus(interface="socketcan", channel="can1", receive_own_messages=False)

    while True:
        time.sleep(0.1)  # Adjust if needed

        # Check for incoming data from client (non-blocking)
        try:
            data = client_socket.recv(1024)
            if data:
                # Decode the CAN message, send it to the CAN bus, and print
                can_message = pickle.loads(data)
                can_bus.send(can_message)
                print(f"Forwarded message to CAN bus: {str(can_message)}")
        except BlockingIOError:
            pass

        # Sending outgoing messages
        new_messages = client_new_messages[addr]
        raw_messages = list(raw_message_buffer)[-new_messages:]
        messages = []
        for raw_message in raw_messages:
            messages.append(pickle.dumps(raw_message))
        data_to_send = pickle.dumps(messages)
        # Send the length of the data first
        length = len(data_to_send)
        client_socket.sendall(length.to_bytes(4, 'big'))
        # Then send the data
        client_socket.sendall(data_to_send)

        # Reset the new messages counter for this client
        client_new_messages[addr] = 0

# Start collection and server threads
threading.Thread(target=collection_thread, daemon=True).start()
threading.Thread(target=server_thread, daemon=True).start()

# Keep the server running
while True:
    time.sleep(0.1)
