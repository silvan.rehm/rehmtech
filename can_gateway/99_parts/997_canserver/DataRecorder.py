import csv
import logging
import threading
import os
from datetime import datetime
import time

class DataRecorder:
    def __init__(self, data_storage, record_dir="~/python/data/TUI", record_dt=0.1):
        self.data_storage = data_storage
        self.record_dir = os.path.expanduser(record_dir)
        self.record_dt = record_dt
        self.recording = False
        self._thread = None

        if not os.path.exists(self.record_dir):
            os.makedirs(self.record_dir)

    def start_recording(self):
        
        self.recording = True
        self._thread = threading.Thread(target=self._record_loop, daemon=True)
        self._thread.start()

    def stop_recording(self):
        self.recording = False
        if self._thread:
            self._thread.join()

    def _record_loop(self):
        filename = os.path.join(self.record_dir, f"{str(datetime.now())}.csv")
        logging.info(f"starting to record in {filename}")
        with open(filename, 'w', newline='') as csvfile:
            # Dynamic headers based on current node ids
            node_ids = list(self.data_storage.keys())
            headers = ["Timestamp"] + [f"{node_id}.{metric}" for node_id in node_ids for metric in ["V_Hi", "I_Hi", "V_Lo", "I_Lo", "m", "mode"]]
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(headers)

            while self.recording:
                try:
                    row = [datetime.now()]
                    for node_id in node_ids:
                        data = self.data_storage[node_id]
                        row.extend([
                            data["V_Hi"][-1] if len(data["V_Hi"]) > 0 else None,
                            data["I_Hi"][-1] if len(data["I_Hi"]) > 0 else None,
                            data["V_Lo"][-1] if len(data["V_Lo"]) > 0 else None,
                            data["I_Lo"][-1] if len(data["I_Lo"]) > 0 else None,
                            data["m"][-1] if "m" in data and len(data["m"]) > 0 else None,
                            data["mode"][-1] if "mode" in data and len(data["mode"]) > 0 else "N/A"
                        ])
                    csvwriter.writerow(row)
                except Exception as e:
                    logging.error(e)
                    self.recording = False
                    break           
                time.sleep(self.record_dt)
        logging.info("Stopped recording")
