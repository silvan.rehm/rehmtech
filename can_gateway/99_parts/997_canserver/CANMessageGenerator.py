from can import Message
import struct
import logging

class CANMessageGenerator:

    set_ids={
        "set_duty_cycle": 0x10,
        "set_PWM_freq": 0x11
    }

    def __init__(self):
        pass

    def float_to_bytes(self, value):
        # int_val = int(value * 0xFFFF)
        packed_data = struct.pack('>H', int(value))        
        return list(packed_data)

    def lookup_set_id(self, cmd):          
        set_id = self.set_ids.get(cmd)
        if set_id is None:
            logging.error(f"Unknown command: {cmd}")
            return 0xFF  # or return None, depending on how you want to handle it later
        return set_id

    def set_duty_cycle(self, value):
        # DC is between 0 and 0xFFFF 
        return [self.lookup_set_id("set_duty_cycle")] + self.float_to_bytes(value/100.0*0xFFFF)
    
    def set_PWM_freq(self, value):
        # input is in kHz, we set in 4 Hz (input: 100 -> 100 kHz -> we send 25_000)
        return [self.lookup_set_id("set_PWM_freq")] + self.float_to_bytes(value*250.0)

    def generate_msg(self, cmd, config) -> Message:
        # Dynamically get the method from the generator
        # If method does not exist, AttributeError will be raised
        logging.info(f"cmd: {cmd}, config:{config}")
        value = config.get(cmd,None)
        func = getattr(self, cmd, None)
        if not all([func, value]):
            logging.error(f"No method or value found for: {cmd}")
            return None
        cmd_id = 0x0020  # this is for setting values. 
        data = func(value)
        msg_id = cmd_id << 8 | int(config['target_node'])
        return Message(arbitration_id=msg_id, data=data)
