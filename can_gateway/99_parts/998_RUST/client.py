import socket

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("192.168.1.229", 7878))
    
    while True:
        data = s.recv(1024)
        if not data:
            break
        print("Received:", data.decode())

if __name__ == "__main__":
    main()