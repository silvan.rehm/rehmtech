extern crate socketcan;

use socketcan::socket::CanSocket;
use crate::socketcan::Socket;

fn main() {
    // Open the CAN interface named "can1"
    let can_socket = CanSocket::open("can1").expect("Failed to open CAN interface");

    loop {
        match can_socket.read_frame() {
            Ok(frame) => {
                println!("{:?}", frame);
            }
            Err(e) => {
                eprintln!("Error reading CAN frame: {}", e);
            }
        }
    }
}
