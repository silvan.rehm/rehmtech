extern crate socketcan;

use socketcan::socket::CanSocket;
use socketcan::Socket; 
use std::io::Write;
use std::net::{TcpListener, TcpStream};

fn handle_client(mut stream: TcpStream, can_socket: &socketcan::CanSocket) {
    loop {
        match can_socket.read_frame() {
            Ok(frame) => {
                let message = format!("{:?}\n", frame);
                if let Err(e) = stream.write_all(message.as_bytes()) {
                    println!("Error writing to stream: {:?}", e);
                    break;
                }
            },
            Err(e) => println!("Error reading CAN frame: {:?}", e),
        }
    }
}

fn main() {
    let listener = TcpListener::bind("192.168.1.229:7878").unwrap();

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let can_socket = CanSocket::open("can1").expect("Failed to open CAN interface");
                handle_client(stream, &can_socket);
            }
            Err(e) => {
                println!("Error: {}", e);
            }
        }
    }
}
