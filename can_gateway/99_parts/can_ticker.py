import can
import struct
import time
import os 

bus = can.Bus(interface="socketcan", channel="can1", receive_own_messages=False)
msg = bus.recv(1.0)
_id = msg.arbitration_id
_data = msg.data.hex()

print(f"got {_data} from {_id}")


N = 10_000
print(f"receiving {N} messages")
t0 = time.time()
for i in range(N):
    msg = bus.recv(1.0)
t1 = time.time()
dt = t1-t0
print(f"received {i+1} messages in {dt} seconds")
print(f"message frequency: {N/dt}")

