import sys

from bluepy.btle import DefaultDelegate, Peripheral, UUID


class MyDelegate(DefaultDelegate):
    # Constructor (run once on startup)
    def __init__(self, params):
        print("__init__ called")
        DefaultDelegate.__init__(self)

    # func is called on notifications
    def handleNotification(self, cHandle, data):
        print("Notification from Handle: 0x" + format(cHandle, '02X') + " Value: " + format(ord(data[0])))


dev_name_uuid = UUID(0x2A00)

if len(sys.argv) != 2:
    print("Fatal, must pass device address:", sys.argv[0], "<device address="">")
    quit()

p = Peripheral(sys.argv[1])
p.setMTU(251)
ch = p.getCharacteristics(uuid=dev_name_uuid)[0]
if ch.supportsRead():
    print(ch.read())

# services = p.getServices()
# displays all services
# for service in services:
#     print(service)
#     for chara in service.getCharacteristics():
#         print(chara)
service = p.getServiceByUUID("abb0")
char1 = service.getCharacteristics()

for ch in char1:
    print("{}: {}".format(ch, ch.read()))


# for chara in service.getCharacteristics():
#     print(chara)

# characteristics = p.getCharacteristics()
# for characteristic in characteristics:
#     ch_uuid = str(characteristic.uuid)
#     if not ch_uuid.__contains__("abb"):
#         continue
#     print("-" * 20)
#     print(characteristic)
#     print(characteristic.uuid)
#     try:
#         print(characteristic.read())
#     except:
#         print("  not readable  ")
#         pass
#     print("-"*20)
#     print("")
#
# p.setDelegate(MyDelegate(p))
#
# print("starting loop")
# while True:
#     if p.waitForNotifications(10.0):
#         # handleNotification() was called
#         continue
#     print(".")
