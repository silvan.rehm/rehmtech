import tkinter as tk

def update_text_field(value, text_field):
    text_field.delete(0, "end")
    text_field.insert(0, value)

def update_slider(text_field, slider):
    value = text_field.get()
    try:
        value = int(value)
    except ValueError:
        pass
    slider.set(value)

def say_hi():
    print("hi!")

def update_ui():
    # Code to update the UI
    print("hihi")
    slider1.set(slider1.get()/2)
    root.after(1000, update_ui)  # Reschedule the function to be executed after 1000 milliseconds





root = tk.Tk()
root.title("Tkinter GUI Example")
root.geometry("800x600")

# Listbox
listbox = tk.Listbox(root)
listbox.pack(side="left", fill="y")
listbox.insert("end", "PCBA")
listbox.insert("end", "PCBB")
listbox.insert("end", "PCBC")

# Dropdown menu
options = ["Option 1", "Option 2", "Option 3"]
var = tk.StringVar()
var.set(options[0])
dropdown = tk.OptionMenu(root, var, *options)
dropdown.pack(side="top", pady=10)

# Sliders and Text Fields
slider_frame1 = tk.Frame(root)
slider_frame1.pack()
tk.Label(slider_frame1, text="Modulation Index").pack(side="left")
slider1 = tk.Scale(slider_frame1, from_=0, to=100, orient="horizontal", showvalue=0, command=lambda value: update_text_field(value, text_field1))
slider1.pack(side="left")
text_field1 = tk.Entry(slider_frame1, width=5)
text_field1.insert(0, "0")
text_field1.pack(side="left")
text_field1.bind("<Return>", lambda event: update_slider(text_field1, slider1))
text_field1.bind("<KP_Enter>", lambda event: update_slider(text_field1, slider1))


# slider2 = tk.Scale(root, from_=0, to=100, orient="horizontal", showvalue=0, command=lambda value: update_text_field(value, text_field2))
# slider2.pack()
# text_field2 = tk.Entry(root, width=5)
# text_field2.insert(0, "0")
# text_field2.pack()
# text_field2.bind("<Return>", lambda event: update_slider(text_field2, slider2))

# slider3 = tk.Scale(root, from_=0, to=100, orient="horizontal", showvalue=0, command=lambda value: update_text_field(value, text_field3))
# slider3.pack()
# text_field3 = tk.Entry(root, width=5)
# text_field3.insert(0, "0")
# text_field3.pack()
# text_field3.bind("<Return>", lambda event: update_slider(text_field3, slider3))
# text_field3.bind("<FocusOut>", lambda event: update_slider(text_field3, slider3))


# Button
button = tk.Button(root, text="Say Hi!", command=say_hi)
button.pack(side="right")
root.after(1000, update_ui)  # Schedule the function to be executed after 1000 milliseconds
root.mainloop()
