# Additional Values

- measure and submit temperature
hmm aggregator PV is really only of PV.. 

we can reuse the uploader

i2cthread for getting values from i2c

how to organize?
we need a node_id for the masters.
0x1001 -> first master

```python
data_body['tags']['node_id'] = node_id
data_body['tags']['alias'] = alias
data_body['fields']['V_Hi'] = averages[node_id]['V_Hi_ave']
data_body['fields']['I_Hi'] = averages[node_id]['I_Hi_ave']
data_body['time'] = "{}".format(datetime.now())
# print(data_body)
self.upload_queue.put(deepcopy(data_body))
```

# TODO:

- local config for UI
- print menu in UI
- general aggregator (needs some structure for CAN IDs)


# Done:

- local config for server


OLD:
# TODO:

- logging in UI
    - header how? create new header / file if we get additional can IDs?
    - drop can IDs when data is older then .. ?
- nice UI



server: only stores a dict of arriving messages in can_msgs

UI: gets can_msgs

UI: gets dict of messages
.. can we pass messages through a json? -> nope! creating new fields 
can_msgs[id][timestamp]
can_msgs[id][data]

>>> msgs[msg.arbitration_id][timestamp] = msg.timestamp
msg.timestamp
>>> msgs[msg.arbitration_id]['timestamp'] = msg.timestamp
>>> msgs[msg.arbitration_id]['data'] = msg.data
>>> json.dumps(msgs)
'{"11141131": {"timestamp": 1591808918.53518, "data": "3b5630421bc5c7bf"}}'
>>> import datetime
>>> datetime.datetime.fromtimestamp(msg.timestamp)
datetime.datetime(2020, 6, 10, 18, 8, 38, 535180)

```python

import can
import struct

bus = can.Bus(interface="socketcan", channel="can0", receive_own_messages=False)

while 1:
    msg = bus.recv(2.0)
    # print(msg)
    data = msg.data
    f1 = struct.unpack('f', bytes(data[0:4]))[0]
    print("%.3f" % f1)


data = msg.data  # byte array 0..8 bytes
logging.info(json.dumps(msg))
_id = msg.arbitration_id
node_id = _id & 0xFF
function_id = _id >> 8 & 0xFFFF

if function_id == 0xAA00:  # Hi values
    try:
        V_Hi = struct.unpack('f', bytes(data[0:4]))[0]
    except Exception as e:
        # logging.info(e)
        V_Hi = 0
    try:
        I_Hi = struct.unpack('f', bytes(data[4:8]))[0]
    except Exception as e:
        # logging.info(e)
        I_Hi = 0
    logging.info("Node %i: V_Hi %.3f / I_Hi: %.3f" % (node_id, V_Hi, I_Hi))

if function_id == 0xAA01:  # Lo values
    try:
        V_Lo = struct.unpack('f', bytes(data[0:4]))[0]
    except Exception as e:
        # logging.info(e)
        V_Lo = 0
    try:
        I_Lo = struct.unpack('f', bytes(data[4:8]))[0]
    except Exception as e:
        # logging.info(e)
        I_Lo = 0
    logging.info("Node %i: V_Lo %.3f / I_Lo: %.3f" % (node_id, V_Lo, I_Lo))


``` 