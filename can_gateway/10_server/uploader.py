import logging
import threading
import time
from queue import Queue
from influxdb import InfluxDBClient

class Uploader(threading.Thread):

    def __init__(self, config, upload_queue: Queue):
        threading.Thread.__init__(self, name="uploader")
        self.running = True
        self.upload_queue = upload_queue
        self.config = config['uploader']
        print(self.config)
        host = self.config['influx']['host']
        port = self.config['influx']['port']
        db_user_name = self.config['influx']['db_user_name']
        db_user_password = self.config['influx']['db_user_password']
        db_name = self.config['influx']['db_name']
        self.client = InfluxDBClient(host, port, db_user_name, db_user_password, db_name)
        logging.info("uploader init")

    def stop(self):
        logging.info("uploader stopped")
        self.running = False

    def upload(self):
        # logging.info("uploading!")
        items = []
        while not self.upload_queue.empty():
            items.append(self.upload_queue.get())
        print(items)
        self.client.write_points(items)

    def run(self):
        logging.info("uploader starting")
        while self.running:
            time.sleep(self.config['time'])
            try:
                self.upload()
            except Exception as e:
                print(e)
