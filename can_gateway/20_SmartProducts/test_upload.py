import yaml
import json
from datetime import datetime
from influxdb import InfluxDBClient

db = yaml.safe_load(open("the_order.yaml"))
host = db['influx']['host']
port = db['influx']['port']
db_user_name = db['influx']['db_user_name']
db_user_password = db['influx']['db_user_password']
db_name = db['influx']['db_name']

# influx:
#   host: 192.168.1.196
#   port: 8086
#   admin_user: admin
#   admin_password: admin
#   db_name: db0
#   db_user: jean_cloud
#   db_password: can_dem

json_body = json.load(open("body.json"))
json_body['measurement'] = "testing_2"
json_body['tags']['node_id'] = "0xFF"
json_body['tags']['alias'] = "manual"
json_body['fields']['temperature'] = 21.00
json_body['fields']['relative_humidty'] = 50.00

json_body['time'] = "{}".format(datetime.now())

print(json_body)

client = InfluxDBClient(host, port, db_user_name, db_user_password, db_name)
client.write_points([json_body])
print(client.get_list_database())

