import logging
import threading
import time
from queue import Queue
import can

class CanCollector(threading.Thread):

    def __init__(self, config, queue: Queue):
        threading.Thread.__init__(self, name="can_collector")
        self.running = True
        self.queue = queue
        self.config = config['can_collector']
        logging.info("can collector init")
        self.bus = can.Bus(interface="socketcan", channel="can0", receive_own_messages=False)



    def stop(self):
        logging.info("can collector stopped")
        self.running = False
#
#     can_collector:
#     mocking: on
#     can_timeout: 2  # time out in seconds
#     fake_can_msgs:
#     - id: 0xaa0020
#     data: [0x19, 0x3, 0x0, 0x0, 0xc8, 0x1f, 0x0, 0x0]
#
#
# - id: 0xaa001e
# data: [0xa7, 0x8, 0x0, 0x0, 0x3d, 0xc, 0x0, 0x0]
# - id: 0xaa001f
# data: [0x33, 0xb, 0x0, 0x0, 0x11, 0x9, 0x0, 0x0]

    def mocking(self):
        for fake_msg in self.config['fake_can_msgs']:
            fake_msg['data'] = bytearray(fake_msg['data'])
            self.queue.put(fake_msg)

    def run(self):
        logging.info("can collector starting")
        while self.running:
            try:
                msg = self.bus.recv(self.config['can_timeout'])
                self.queue.put(msg)
            except Exception as e:
                print(e)

            # id = msg.arbitration_id

            # time.sleep(self.config['can_timeout'])
            # # print(".")
            # if self.config['mocking']:
            #     self.mocking()
            # else:
            #     self.queue.put("1")
