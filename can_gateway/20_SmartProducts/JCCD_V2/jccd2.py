import json
import logging
import time
from queue import Queue

import yaml

import aux_jccd2 as aux
from aggregator import Aggregator
from can_collector import CanCollector
from uploader import Uploader

# config = yaml.safe_load(open("config/jccd2.yml"))
config = aux.load_config("config/jccd2.yml",  "init_jccd2_local.yaml")
data_body = json.load(open("config/body.json"))
data_body['measurement'] = config['measurement']
aux.prepare_logging(config)

logging.info("jccd2 starting")

can_queue = Queue()
upload_queue = Queue()

can_collector = CanCollector(config, can_queue)
can_collector.setDaemon(True)
can_collector.start()

aggregator = Aggregator(config, can_queue, upload_queue, data_body)
aggregator.setDaemon(True)
aggregator.start()

uploader = Uploader(config, upload_queue)
uploader.setDaemon(True)
uploader.start()

while True:
    time.sleep(1)

can_collector.stop()
aggregator.stop()
uploader.stop()
