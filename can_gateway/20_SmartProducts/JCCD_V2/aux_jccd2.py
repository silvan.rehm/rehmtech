import logging
import os
import shutil
import sys
import time

import yaml


def load_config(init_config_path, template_config_path):
    db = yaml.safe_load(open(init_config_path))
    local_config_dir = os.path.expanduser(db['local_config_dir'])
    if not os.path.exists(local_config_dir):
        os.makedirs(local_config_dir)
    # INIT
    local_db = prepare_load_local(local_config_dir, db['local_config_name'],template_config_path)
    db.update(local_db)
    return db


def prepare_load_local(local_config_dir, _local_fn, template_fn):
    local_fn = os.path.join(local_config_dir, _local_fn)
    if os.path.isfile(local_fn):  # if we have a local file, we load it
        local_db = yaml.safe_load(open(local_fn))
    else:  # if we don't have a local file, we copy the template and load it
        shutil.copyfile(template_fn, local_fn)
        print("First Time Running detected!")
        print("Copied over default config to {}".format(local_fn))
        print("edit this file and restart me")
        sys.exit(0)

        local_db = yaml.load(open(local_fn))
    return local_db


def prepare_logging(config):
    log_dir = os.path.expanduser(config['log_dir'])

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)s] [%(levelname)s]  %(message)s")
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.NOTSET)

    file_handler = logging.FileHandler(os.path.join(log_dir, "%i.txt" % (time.time() * 1000.0)))
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    log = logging.getLogger('pymodbus')
    log.setLevel(logging.ERROR)
    log = logging.getLogger('matplotlib')
    log.setLevel(logging.ERROR)
    log = logging.getLogger('can')
    log.setLevel(logging.ERROR)
    log = logging.getLogger('influxdb')
    log.setLevel(logging.ERROR)
