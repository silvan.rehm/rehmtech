import can
import yaml
import os
from influxdb import InfluxDBClient
import json
from _datetime import datetime

# Load configuration file
db = yaml.safe_load(open("config/the_order.yaml"))
# Load data body
json_body = json.load(open("config/body.json"))

print(db)
print(json_body)

# Extract values from config
host = db['influx']['host']
port = db['influx']['port']
db_user_name = db['influx']['db_user_name']
db_user_password = db['influx']['db_user_password']
db_name = db['influx']['db_name']

# Connecto to CAN bus
bus = can.Bus(interface="socketcan", channel="can0", receive_own_messages=False)
# Connecto to InfluxDB
client = InfluxDBClient(host, port, db_user_name, db_user_password, db_name)

# set the measurement of the data body
json_body['measurement'] = db['measurement']

while True:
    try:  # trying to get a message from the CAN bus
        msg = bus.recv(2.0)  # timeout: 2 seconds
        data = msg.data.hex()  # extracting the data
        _id = msg.arbitration_id  # extrating the ID
    except Exception as e:
        print(e)
        continue  # in case this did not work we go back and try again
    try:
        _node_id = int(_id) & 0xFF # nodeID is the last two bytes
        node_id = "{0:#0{1}x}".format(_node_id, 4)  # into nice looking strings (i.e. 0x0A)
        if node_id not in db['sensor_dict']:  # if this is a new node
            db['sensor_dict'][node_id] = {}  # prepare dict
            alias = node_id  # in case there are now infos, we use the ID as alias
            if node_id in db['node_infos']: # if we have the alias, we use it
                alias = db['node_infos'][node_id]['alias']
            db['sensor_dict'][node_id]['alias'] = alias
            print("new node!{}({})".format(alias, node_id))
    except Exception as e:
        print(e)
        continue
    try:
        # Converting bytes-data from the MCU to floating point data
        # Data is in .01 °C and 0.01 % RH
        temp_bytes = bytes.fromhex(data[0:8])  # getting bytes from data
        temp = int.from_bytes(temp_bytes, "little") / 100.0
        RH_bytes = bytes.fromhex(data[8:16])
        RH = int.from_bytes(RH_bytes, "little") / 100.0
        db['sensor_dict'][node_id]['RH'] = RH  # storing in database
        db['sensor_dict'][node_id]['temp'] = temp

        # Setting values of data body
        json_body['tags']['node_id'] = node_id
        json_body['tags']['alias'] = db['sensor_dict'][node_id]['alias']
        json_body['fields']['temperature'] = temp
        json_body['fields']['relative_humidty'] = RH
        json_body['time'] = "{}".format(datetime.now())
        # sending data body
        client.write_points([json_body])

        # mini UI: grid with all the sensors
        # os.system('clear')
        for node_id, values in db['sensor_dict'].items():
            temp = values['temp']
            RH = values['RH']
            alias = values['alias']
            print("{}: {} °C / {} % RH".format(alias, temp, RH))
    except Exception as e:
        print(e)
# That's it.. 77 lines with commments!