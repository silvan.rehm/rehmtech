GET STARTED:


Smart Products Demonstrator:

Goal: Measure T,RH of many sensor boxes and submit to cloud

Steps:
- display locally in
- upload (needs cloud, wait for epiphany)

# display locally

- grab can and spit out -> works!
- organize in a dict
    - key: node ID
    - values:
        - alias: display name (from )
        - RH
        - Temp

- aliases done
- test upload
- connecting database
- uploading every message received 

starting with documentation

# JCCD V2

Next level JCCD:

- Separate thread for collecting the messages
- aggregating data (i.e. 10 seconds)
- sending only aggregated data (i.e. every minute)

- can_collector: collects can messages and packs them into -> can_can
- aggregator: grabs can-messages from can_can, aggregates them and puts them into a data
- uploader: periodically uploads data 

skeleton done, fleshing out details
- can_collector stores dicts with id (integer) and data (list of integers)
    -> mock up works, needs real data
- aggregator:
    - evaluates data according to id and stores it in a dict: "0x1E":{"RH_list":[], "T_list":[]}
    - averages over all sensors and values
    - stores one data_json per sensor in the upload_queue
- uploader:
    - goes through upload_queue and uploads data

no more mockup.. time to log for real!

aggregating and upload works .. but it only uploads the last value.. maaybeee... yes, we were only saving the last one.. again!

changed.. not helping. 
we are also uploading 5x the same values!

[{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:42:54.480696', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:42:54.480812', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:42:54.480886', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:04.492854', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:04.492978', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:04.493062', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:14.504975', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:14.505095', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:14.505176', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:24.517027', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:24.517149', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:24.517234', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:34.529095', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:34.529216', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}, {'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:43:34.529298', 'fields': {'temperature': 29.776, 'relative_humidity': 0, 'relative_humidty': 22.822999999999997}}]

but we are putting the right values into the queue
aggregating 0x20
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x20', 'alias': 'shed_outside'}, 'time': '2020-12-16 15:49:35.549008', 'fields': {'temperature': 7.778000000000001, 'relative_humidty': 82.95}}
aggregating 0x1e
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1e', 'alias': 'shed_internal'}, 'time': '2020-12-16 15:49:35.554138', 'fields': {'temperature': 23.198999999999995, 'relative_humidty': 30.93}}

aggregating 0x1f
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:49:35.556071', 'fields': {'temperature': 29.729999999999997, 'relative_humidty': 22.856}}

aggregating 0x20
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x20', 'alias': 'shed_outside'}, 'time': '2020-12-16 15:49:45.568061', 'fields': {'temperature': 7.784000000000001, 'relative_humidty': 82.93200000000002}}
aggregating 0x1e
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1e', 'alias': 'shed_internal'}, 'time': '2020-12-16 15:49:45.568349', 'fields': {'temperature': 23.195999999999998, 'relative_humidty': 30.93}}
aggregating 0x1f
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:49:45.568587', 'fields': {'temperature': 29.736999999999995, 'relative_humidty': 22.854999999999997}}



[
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}, 
{'measurement': 'testing_jccdV2', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 15:53:58.312937', 'fields': {'temperature': 29.722, 'relative_humidty': 22.859}}]

they're all the last reported value.. we are overwriting values!

deepcopy helped

but shed_internal is not showing up.. but it should be uploaded!

[
{'measurement': 'demo_shed', 'tags': {'node_id': '0x20', 'alias': 'shed_outside'}, 'time': '2020-12-16 16:08:19.047738', 'fields': {'temperature': 7.953000000000001, 'relative_humidty': 82.447}}, 
{'measurement': 'demo_shed', 'tags': {'node_id': '0x1e', 'alias': 'shed_internal'}, 'time': '2020-12-16 16:08:19.048392', 'fields': {'temperature': 23.227, 'relative_humidty': 30.935000000000002}}, 
{'measurement': 'demo_shed', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 16:08:19.049055', 'fields': {'temperature': 29.755999999999993, 'relative_humidty': 22.853}}, 
{'measurement': 'demo_shed', 'tags': {'node_id': '0x20', 'alias': 'shed_outside'}, 'time': '2020-12-16 16:08:29.060647', 'fields': {'temperature': 7.959999999999999, 'relative_humidty': 82.45400000000001}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1e', 'alias': 'shed_internal'}, 'time': '2020-12-16 16:08:29.061049', 'fields': {'temperature': 23.226, 'relative_humidty': 30.932}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 16:08:29.061458', 'fields': {'temperature': 29.751000000000005, 'relative_humidty': 22.859}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x20', 'alias': 'shed_outside'}, 'time': '2020-12-16 16:08:39.073722', 'fields': {'temperature': 7.958, 'relative_humidty': 82.45900000000002}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1e', 'alias': 'shed_internal'}, 'time': '2020-12-16 16:08:39.074383', 'fields': {'temperature': 23.228999999999996, 'relative_humidty': 30.933000000000003}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 16:08:39.075048', 'fields': {'temperature': 29.768, 'relative_humidty': 22.856}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x20', 'alias': 'shed_outside'}, 'time': '2020-12-16 16:08:49.087367', 'fields': {'temperature': 7.965000000000001, 'relative_humidty': 82.447}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1e', 'alias': 'shed_internal'}, 'time': '2020-12-16 16:08:49.088027', 'fields': {'temperature': 23.231, 'relative_humidty': 30.935000000000002}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 16:08:49.088677', 'fields': {'temperature': 29.769999999999992, 'relative_humidty': 22.845999999999997}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x20', 'alias': 'shed_outside'}, 'time': '2020-12-16 16:08:59.101000', 'fields': {'temperature': 7.964999999999999, 'relative_humidty': 82.429}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1e', 'alias': 'shed_internal'}, 'time': '2020-12-16 16:08:59.101648', 'fields': {'temperature': 23.232, 'relative_humidty': 30.933}}, {'measurement': 'demo_shed', 'tags': {'node_id': '0x1f', 'alias': 'shed_3dprinter'}, 'time': '2020-12-16 16:08:59.102342', 'fields': {'temperature': 29.773000000000003, 'relative_humidty': 22.843}}]

making new measurement: jccd2_2020-12-17_A

dfq, it works in new measurement?

New measurement with corrected name for humidity!

# NEXT: Config file

Goal: no longer in git! 
running local: in home
running in container: from config/

I think the preferred way to do this is through environment variables. If you're creating your Python app from a Dockerfile, you could specify the 'ENV' directive:

https://docs.docker.com/engine/reference/builder/#env

Dockerfile:

...
ENV AM_I_IN_A_DOCKER_CONTAINER Yes
which could then be read from your app with something like:

python_app.py:

import os

SECRET_KEY = os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False)

if SECRET_KEY:
    print('I am running in a Docker container') 

# BACKLOG

## continuous query:
create_continuous_query(name, select, database=None, resample_opts=None)
Create a continuous query for a database.

Parameters:	
name (str) – the name of continuous query to create
select (str) – select statement for the continuous query
database (str) – the database for which the continuous query is created. Defaults to current client’s database
resample_opts (str) – resample options
Example:	
>> select_clause = 'SELECT mean("value") INTO "cpu_mean" ' \
...                 'FROM "cpu" GROUP BY time(1m)'
>> client.create_continuous_query(
...     'cpu_mean', select_clause, 'db_name', 'EVERY 10s FOR 2m'
... )
>> client.get_list_continuous_queries()
[
    {
        'db_name': [
            {
                'name': 'cpu_mean',
                'query': 'CREATE CONTINUOUS QUERY "cpu_mean" '
                        'ON "db_name" '
                        'RESAMPLE EVERY 10s FOR 2m '
                        'BEGIN SELECT mean("value") '
                        'INTO "cpu_mean" FROM "cpu" '
                        'GROUP BY time(1m) END'
            }
        ]
    }
]