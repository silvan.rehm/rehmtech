import json

import logging
import os
import socket
import struct
import sys
import threading
import time
import getpass
import uuid
import subprocess
from collections import OrderedDict
import shutil

import yaml

def load_config():
    db = yaml.safe_load(open("init_can_gateway_UI.yaml"))
    local_config_dir = os.path.expanduser(db['local_config_dir'])
    if not os.path.exists(local_config_dir):
        os.makedirs(local_config_dir)
    local_fn = os.path.join(local_config_dir, db['local_config_name'])
    if os.path.isfile(local_fn):  # if we have a local file, we load it
        local_db = yaml.safe_load(open(local_fn))
    else:  # if we don't have a local file, we copy the template and load it
        shutil.copyfile(os.path.join(db['local_config_name']), local_fn)
        local_db = yaml.safe_load(open(local_fn))
    if not local_db['values_updated']:
        print("update {} first!".format(local_fn))
        sys.exit(0)
    db.update(local_db)
    return db

# def load_config():
#     db = yaml.safe_load(open("config/init_can_gateway_server.yaml"))
#     local_config_dir = os.path.expanduser(db['local_config_dir'])
#     if not os.path.exists(local_config_dir):
#         os.makedirs(local_config_dir)
#     local_fn = os.path.join(local_config_dir, db['local_config_name'])
#     if os.path.isfile(local_fn):  # if we have a local file, we load it
#         local_db = yaml.safe_load(open(local_fn))
#     else:  # if we don't have a local file, we copy the template and load it
#         shutil.copyfile(os.path.join("config",db['local_config_name']), local_fn)
#         local_db = yaml.safe_load(open(local_fn))
#     if not local_db['values_updated']:
#         print("update {} first!".format(local_fn))
#         sys.exit(0)
#     db.update(local_db)
#     return db

def aux_connect_to_server(db):
    db['dev']['socket'] = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    db['user_name'] = getpass.getuser()
    db['hostname'] = socket.gethostname()
    db['UUID'] = "%s" % uuid.uuid1()
    server = db['server']
    logging.info("connecting to %s on port %i" % (server['IP'], server['port']))
    try:
        db['dev']['socket'].connect((server['IP'], server['port']))
        logging.info("worked!")
    except ConnectionRefusedError as e:
        logging.info("server not responding, trying to start it")
        logging.exception(e)
        sys.exit(0)
        # p = subprocess.Popen(["ssh", "%s@%s" %(db['server']['user'], db['server']['IP']), "screen", "-dmS", "test", "sh", "start_PC08.sh"])
        # time.sleep(db['wait_for_server'])
        # try:
        #     db['dev']['socket'].connect((server['IP'], server['port']))
        # except ConnectionRefusedError as e:
        #     logging.exception(e)
        #     sys.exit(0)


def prepare_logging(db):
    log_dir = os.path.expanduser(db['log_dir'])

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)s] [%(levelname)s]  %(message)s")
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.NOTSET)

    file_handler = logging.FileHandler(os.path.join(log_dir, "%i.txt" % (time.time() * 1000.0)))
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    log = logging.getLogger('pymodbus')
    log.setLevel(logging.ERROR)
    log = logging.getLogger('matplotlib')
    log.setLevel(logging.ERROR)


def send_dict(_socket, _dict, _talking):
    while _talking:  # while the socket is busy
        time.sleep(50e-3)  # we wait
    _talking = True  # grab socket
    time.sleep(50e-3)
    _socket.sendall(json.dumps(_dict).encode())
    data = ""
    # _socket.setblocking(False)
    _socket.settimeout(0.1)
    while True: 
        # print("getting more")
        try:
            buffer = _socket.recv(4096).decode()
        except:
            buffer = ""
        # print(f"got {len(buffer)}")
        if buffer == "":
            break
        data += buffer
        # print(buffer)
    # _socket.setblocking(True)

    time.sleep(50e-3)
    _talking = False  # release socket
    return data


def get_update(db):
    _dict = {"msg": "just checking"}
    # if "for_server" in db:
    #     _dict['from_UI'] = db['for_server']
    #     db.pop('for_server', None)
    # #     for_server
    # data_json = {}
    data = send_dict(db['dev']['socket'], _dict, db['talking'])
    try:
        data_json = json.loads(data)
        db['server_db'].update(data_json)
    except Exception as e:
        logging.exception(e)
        # logging.exception("ERROR WITH DECODING JSON")
        # logging.exception("|")
        logging.exception(data)
        # logging.exception("|")


def prepare_new_node_id(db, node_id):
    """
    adds all the values to the new node ID
    :param db:
    :param node_id:
    :return:
    """
    db['can_ids']["%s" % node_id] = {can_value: 0 for can_value in db['can_values']}


def process_can_msgs(db):
    for _id, can_msg in db['server_db']['can_msgs'].items():
        _node_id = int(_id) & 0xFF
        node_id = "{0:#0{1}x}".format(_node_id, 4)   # into nicelooking strings (i.e. 0x0A)
        if node_id not in db['can_ids']:
            prepare_new_node_id(db, node_id)
        function_id = int(_id) >> 8 & 0xFFFF
        data = can_msg['data']
        timestamp = can_msg['timestamp']
        if function_id == 0xAA00:  # Hi values
            db['can_ids']["%s" % node_id]['V_Hi'] = struct.unpack('f', bytes.fromhex(data[0:8]))[0]
            db['can_ids']["%s" % node_id]['I_Hi'] = struct.unpack('f', bytes.fromhex(data[8:16]))[0]
            db['can_ids']["%s" % node_id]['P_Hi'] = db['can_ids']["%s" % node_id]['V_Hi'] * db['can_ids']["%s" % node_id]['I_Hi']
        if function_id == 0xAA01:  # Hi values
            db['can_ids']["%s" % node_id]['V_Lo'] = struct.unpack('f', bytes.fromhex(data[0:8]))[0]
            db['can_ids']["%s" % node_id]['I_Lo'] = struct.unpack('f', bytes.fromhex(data[8:16]))[0]
            db['can_ids']["%s" % node_id]['P_Lo'] = db['can_ids']["%s" % node_id]['V_Lo'] * db['can_ids']["%s" % node_id]['I_Lo']
        # db['can_ids']
        P_Hi = abs(db['can_ids']["%s" % node_id]['P_Hi'])
        P_Lo = abs(db['can_ids']["%s" % node_id]['P_Lo'])
        try:
            if P_Hi > P_Lo :
                db['can_ids']["%s" % node_id]['eff'] = P_Lo / P_Hi * 100.0
            else: 
                db['can_ids']["%s" % node_id]['eff'] = P_Lo / P_Hi * 100.0
        except:
            db['can_ids']["%s" % node_id]['eff'] = 0.0


    """
    sorting can msgs according to source and value
    :param db:
    :return:
    """
    pass


def print_menu(db):
    os.system('clear')
    print("can_gateway UI")
    can_msgs = db['server_db']['can_msgs']
    space1 = 10
    line = "".join(
        [can_value.ljust(space1 // 2).rjust(space1) for can_value in db['can_values']] )
    space2 = 10
    print(" " * space2 + line)
    can_ids_ord = OrderedDict(sorted(db['can_ids'].items()))
    format = "%.2f"
    for can_id, data in can_ids_ord.items():
        line = ("%s:" % can_id).ljust(space2)
        line += "".join(
            [(format % data[can_value]).ljust(space1 // 2).rjust(space1) for can_value in db['can_values']] )
        print(line)

    # print(db['server_db'])
    #node_id = _id & 0xFF
        # function_id = _id >> 8 & 0xFFFF
        #
        # if function_id == 0xAA00:  # Hi values
        #     try:
        #         V_Hi = struct.unpack('f', bytes(data[0:4]))[0]
        #     except Exception as e:
        #         # logging.info(e)
        #         V_Hi = 0
        #     try:
        #         I_Hi = struct.unpack('f', bytes(data[4:8]))[0]
        #     except Exception as e:
        #         # logging.info(e)
        #         I_Hi = 0
        #     logging.info("Node %i: V_Hi %.3f / I_Hi: %.3f" % (node_id, V_Hi, I_Hi))
        #
        # if function_id == 0xAA01:  # Lo values
        #     try:
        #         V_Lo = struct.unpack('f', bytes(data[0:4]))[0]
        #     except Exception as e:
        #         # logging.info(e)
        #         V_Lo = 0
        #     try:
        #         I_Lo = struct.unpack('f', bytes(data[4:8]))[0]
        #     except Exception as e:
        #         # logging.info(e)
        #         I_Lo = 0
        #     logging.info("Node %i: V_Lo %.3f / I_Lo: %.3f" % (node_id, V_Lo, I_Lo))


def shut_down(db):
    logging.info("shutting down")
    db['running'] = False
    HOST = db['server_config']['host']
    PORT = db['server_config']['port']
    # connecting to self to close
    _socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    _socket.connect((HOST, PORT))
    time.sleep(1.1)
    _socket.close()
    logging.info("still alive threads:")
    for thread in threading.enumerate():
        logging.info(thread)
    sys.exit(0)


def cmd_enable(db):
    _raw = input("who to enable? (0 -> all, 1..255 -> specific, other values: cancel")
    try:
        _target = int(_raw)
        if 0 <= _target <= 255:
            _dict = {"enable_node": _target}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i" % _target)
    except Exception as e:
        logging.exception(e)
    return None


def cmd_disable(db):
    _raw = input("who to DISABLE? (0 -> all, 1..255 -> specific, other values: cancel")
    try:
        _target = int(_raw)
        if 0 <= _target <= 255:
            _dict = {"disable_node": _target}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i" % _target)
    except Exception as e:
        logging.exception(e)
    return None


def set_mode(db):
    _raw1 = input("who to set state? (0 -> all, 1..255 -> specific, other values: cancel")
    _raw2 = input("which state? (00:stop, 20:idle, 21:auto, 22:fix volt, 23:fix curr 24:fix PMW, 25:MPPT, others:cancel")
    try:
        _target = int(_raw1)
        _mode = int(_raw2, 16)
        if (0 <= _target <= 255) and (0x00 <= _mode <= 0x28):
            _dict = {"set_mode": [_target, _mode]}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i or %i" % (_target, _mode))
    except Exception as e:
        logging.exception(e)
    return None

def set_node_id(db):
    _raw1 = input("who to set node_id? (0 -> all, 1..255 -> specific, other values: cancel")
    _raw2 = input("which nw node_id? (1..255) :")
    try:
        _target = int(_raw1)
        _node_id = int(_raw2, 16)
        if (0 <= _target <= 255) and (0x00 <= _node_id <= 0xFF):
            _dict = {"set_node_id": [_target, _node_id]}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i or %i" % (_target, _node_id))
    except Exception as e:
        logging.exception(e)
    return None


def set_PWM_DC(db):
    _raw1 = input("who to set PWM_DC? (0 -> all, 1..255 -> specific, other values: cancel")
    _raw2 = input("which duty cycle? (0 .. 1.0, others: cancel")
    try:
        _target = int(_raw1)
        _DC = float(_raw2)
        if (0 <= _target <= 255) and (0.0 <= _DC <= 1.0):
            _dict = {"set_PWM_DC": [_target, _DC]}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i or %f" % (_target, _DC))
    except Exception as e:
        logging.exception(e)
    return None


def set_PWM_freq(db):
    _raw1 = input("who to set_PMW_freq? (0 -> all, 1..255 -> specific, other values: cancel")
    _raw2 = input("which freq? (1 .. 200e3 Hz, others: cancel")
    try:
        _target = int(_raw1)
        _freq = float(_raw2)

        if (0 <= _target <= 255) and (0.0 <= _freq <= 200e3):
            _dict = {"set_PWM_freq": [_target, _freq]}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
            logging.info("set pwm freq of %i to %f" %(_target, _freq))
            time.sleep(1.0)
        else:
            logging.info("user cancelled by giving number: %i or %f" % (_target, _freq))
    except Exception as e:
        logging.exception(e)
    return None


def set_current(db):
    _raw1 = input("who to set_current? (0 -> all, 1..255 -> specific, other values: cancel")
    _raw2 = input("which current? (-15 .. 15.0, others: cancel")
    try:
        _target = int(_raw1)
        _current = float(_raw2)
        if (0 <= _target <= 255) and (-15.0 <= _current <= 15.0):
            _dict = {"set_current": [_target, _current]}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i or %f" % (_target, _current))
    except Exception as e:
        logging.exception(e)
    return None

def set_max_current(db):
    _raw1 = input("who to set_max_current? (0 -> all, 1..255 -> specific, other values: cancel")
    _raw2 = input("which current? (-15 .. 15.0, others: cancel")
    try:
        _target = int(_raw1)
        _current = float(_raw2)
        if (0 <= _target <= 255) and (-15.0 <= _current <= 15.0):
            _dict = {"set_max_current": [_target, _current]}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i or %f" % (_target, _current))
    except Exception as e:
        logging.exception(e)
    return None

def set_voltage(db):
    _raw1 = input("who to set_voltage? (0 -> all, 1..255 -> specific, other values: cancel")
    _raw2 = input("which voltage? (5 .. 40, others: cancel")
    try:
        _target = int(_raw1)
        _voltage = float(_raw2)
        if (0 <= _target <= 255) and (5 <= _voltage <= 40.0):
            _dict = {"set_voltage": [_target, _voltage]}
            data = send_dict(db['dev']['socket'], _dict, db['talking'])
        else:
            logging.info("user cancelled by giving number: %i or %f" % (_target, _voltage))
    except Exception as e:
        logging.exception(e)
    return None
