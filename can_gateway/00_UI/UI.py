import logging
import select
import sys

import aux_UI as aux

db = aux.load_config()
aux.prepare_logging(db)
logging.info(db)
aux.aux_connect_to_server(db)

"""
    f: set pwm frequency (float)
    p: set pwm duty cycle 
    m: set mode (int)
    e: enable
    d: disable
"""
menu = {
    "a": aux.set_max_current,
    "d": aux.cmd_disable,
    "e": aux.cmd_enable,
    "f": aux.set_PWM_freq,
    "i": aux.set_current,
    "m": aux.set_mode,
    "p": aux.set_PWM_DC,
    "v": aux.set_voltage,
    "n": aux.set_node_id,
}

while db['running']:
    aux.get_update(db)
    aux.process_can_msgs(db)
    aux.print_menu(db)
    i, o, e = select.select([sys.stdin], [], [], db['dt_refresh'] / 2.0)
    if i:
        _input = sys.stdin.readline().strip()
        if _input in menu:  # if we know the letter
            logging.info("found " + _input)
            menu[_input](db)
        else:  # if we dont
            logging.warning("Unknown letter! |" + _input + "|")
