import os
import json
import logging
import yaml
import logging
from datetime import datetime

def setup_logging(output_dir, level=logging.INFO):
    # Current timestamp in ISO format (down to seconds)
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    
    # Set up logging to file with the current timestamp in its name
    log_file_path = os.path.join(output_dir, f"log_{timestamp}.log")
    
    # Create a logging format that includes the timestamp
    log_format = f'%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s'
    logging.basicConfig(filename=log_file_path, level=level, 
                        format=log_format, datefmt='%Y-%m-%dT%H:%M:%S')

    # Set up logging to console as well
    console = logging.StreamHandler()
    console.setLevel(level)
    formatter = logging.Formatter(log_format, datefmt='%Y-%m-%dT%H:%M:%S')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    logging.info(f"Logging started. Saving logs to {log_file_path}")


def load_config(config_file):
    with open(config_file, 'r') as file:
        return yaml.safe_load(file)

import re
import logging

def parse_header(file_path):
    logging.info(f"parsing {file_path}")
    # Regular expression pattern to match variable declarations.
#    pattern = r"(\w+)\s+(\w+)(?:\[(\d+)\])?\s*=\s*([^;]+);" # V1
    # pattern = r"(\w+)\s+(\w+)\s*\[(\d+)\]\s*=\s*\{([^;]+)\};" #V2
    # pattern = r"(\w+)\s+(\w+)(?:\[(\d+)\])?\s*=\s*([^;]+);" #V3
    pattern = r"(\w+)\s+(\w+)\s*(?:\[(\d+)\])?\s*=\s*(\{[^}]+\}|[^;]+);" #V4



    var_dict = {}
    with open(file_path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            line = line.lstrip()
            logging.debug(f"Processing line: {line.strip()}")

            # Skip lines that are comments / # 
            if line.startswith("//") or line.startswith("#") or not line:
                continue

            # Try to match the line with our pattern.
            match = re.match(pattern, line.strip())
            if match:
                datatype, varname, array_size, value = match.groups()
                variable_info = {"type": datatype, "name": varname}

                if array_size:
                    variable_info["array_size"] = int(array_size)
                    logging.debug(f"Found array variable: {varname} of type {datatype} with size {array_size}.")
                else:
                    logging.debug(f"Found variable: {varname} of type {datatype}.")
                    array_size = 0
                var_dict[varname] = {"datatype": datatype, "array_size": array_size, "default_value": value}
            else:
                logging.info("No match found for line.")
    logging.debug(f"{var_dict}")
    return var_dict


def generate_c_files(variables, types_dict):
    logging.info(f"Generating C files based on {len(variables)} variables.")
    for var_name, var_info in variables.items():
        var_type = var_info["type"]

        # Check if the variable type exists in the type definitions
        if var_type not in types_dict:
            logging.warning(f"Unknown type {var_type}. Please update types.json.")
            continue
    return "dummy.c", "dummy.h"

def generate_python_dict(variables, existing_dict, types_dict):
    logging.info(f"Generating Python dictionary based on {len(variables)} variables.")
     

    return existing_dict  # Update this accordingly based on the parsed variables

def save_to_disk(filename, content):
    logging.info(f"Saving content to {filename}")
    os.makedirs(os.path.dirname(filename), exist_ok=True)  # Create directory if it doesn't exist
    with open(filename, 'w') as f: 
        f.write(content)

def load_variables(output_dir, filename =  "variables.json"):
    py_dict_path = os.path.join(output_dir, filename)
    # Load existing Python dictionary if it exists
    if os.path.exists(py_dict_path):
        with open(py_dict_path, 'r') as file:
            existing_dict = json.load(file)
    else:
        existing_dict = {}
    if "variables" not in existing_dict:
        existing_dict["variables"] = {}
    return existing_dict

TYPE_DEFINITIONS = {
    "_uint8_t":  0x10,
    "uint16_t": 0x11,
    "uint32_t": 0x12,
    "int8_t":   0x20,
    "int16_t":  0x21,
    "int32_t":  0x22,
    "float":    0x32,
    # Add other types if necessary
}
def load_types(output_dir, filename="types.json"):
    py_dict_path = os.path.join(output_dir, filename)
    if os.path.exists(py_dict_path):
        with open(filename, 'r') as file:
            return json.load(file)
    else:
        logging.warning(f"{py_dict_path} not found. Using default type definitions.")
        return TYPE_DEFINITIONS
