import os
import json
import logging
import yaml
import cd_functions as cd

def main(config_file="config.yaml"):

    config = cd.load_config(config_file)

    output_dir = os.path.expanduser(config['outputdir'])
    cd.setup_logging(output_dir, logging.DEBUG)

    
    
    # Step 1: Load existing 
    existing_dict = cd.load_variables(output_dir)
    all_variables = existing_dict['variables']    
    types_dict = cd.load_types(output_dir)

    # Step 2: Parse the header files
    for header_file in config['target_files']:
        if os.path.exists(header_file):            
            parsed_vars = cd.parse_header(header_file)            
            all_variables.update(parsed_vars) 
            logging.info(f"found {len(parsed_vars)} now having {len(all_variables)}")   
        else:
            logging.warning(f"{header_file} not found.")
    logging.debug(f"{all_variables}")
    existing_dict['variables'] = all_variables
    # for header_file in config['target_files']:
    #     if os.path.exists(header_file):
    #         variables = cd.parse_header(header_file)
    #         all_variables.extend(variables)
    #         logging.debug(f"{all_variables}")
    #         folder_name = os.path.splitext(os.path.basename(header_file))[0]
    #         code_output_dir = os.path.join(output_dir, folder_name)

    #         # Step 2: Generate C files
    #         c_file_content, h_file_content = cd.generate_c_files(all_variables, types_dict)

    #         cd.save_to_disk(os.path.join(code_output_dir, "output.c"), c_file_content)
    #         cd.save_to_disk(os.path.join(code_output_dir, "output.h"), h_file_content)
    #     else:
    #         logging.warning(f"{header_file} not found.")

    # Step 3: Update Python dictionary and save it
    
    # new_dict = cd.generate_python_dict(all_variables, existing_dict, types_dict)

    cd.save_to_disk(os.path.join(output_dir, "variables.json"), json.dumps(existing_dict, indent=4))

    logging.info("Process completed!")

if __name__ == "__main__":
    main()
