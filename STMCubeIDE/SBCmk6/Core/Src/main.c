/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include <stdbool.h> // for flash stuff
//#include <stdlib.h> // for max
#include <string.h> // for memcpy
#include "can_functions.h"
#include "can_markers.h"
#include "PCB_definitions.h"
#include "SBC_values.h"
//#include "SBCmk6_14.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define N_ADC 4
#define N_OVERSAMPLE 17// 27 should result in ~2kHz
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

CAN_HandleTypeDef hcan;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */

/*************/
/* CAN STUFF */
/*************/
CAN_RxHeaderTypeDef rxHeader; //CAN Bus Transmit Header
CAN_TxHeaderTypeDef txHeader; //CAN Bus Receive Header
uint8_t canRX[8] = {0,0,0,0,0,0,0,0};  //CAN Bus Receive Buffer
uint8_t canTX[8] = {0,0,0,0,0,0,0,0};  //CAN Bus Transmit Buffer
CAN_FilterTypeDef canfil; //CAN Bus Filter
uint32_t canMailbox; //CAN Bus Mail box variable

/*************/
/* ADC STUFF */
/*************/
uint16_t adc_buf[N_OVERSAMPLE * N_ADC];
uint16_t adc_buf2[N_OVERSAMPLE * N_ADC];
uint32_t avg_vals[N_ADC];
float avg_val = {0.0};
float offset_corr[N_ADC] = {0.0};
float values[N_ADC] = {0.0};
uint16_t can_counter = 0;

/*
 * FLASHY STUFF
 */

#define FLASH_USER_START_ADDR 0x0800FC00 // Address of the calibration data
#define FLASH_USER_END_ADDR   0x0800FFFF // End address of the calibration data








//CalibrationData default_calib_data = {
//    .version = 1,
//    .checksum = 0,  // This will be calculated
//    .uint8_ts = {0xEE},
//    .uint16_ts = {0},
//    .uint32_ts = {0},
//    .floats = {0.0}
//};


uint32_t CalculateChecksum(CalibrationData* data);
bool IsErased(CalibrationData* data);
bool IsValidCalibrationData(CalibrationData* data);
void LoadCalibrationData(CalibrationData* calib_data);
void WriteCalibrationData(CalibrationData *data);

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_CAN_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


void MPPT(){
	// Low m -> more load on the PV -> high m for protecting the f
	// TODO : Limit current
	// TODO: Stop on high voltage
	// TODO: I_Lo changed directin -> need to be check current controller

	if (V_Hi > V_Hi_shutdown){
		disable_PWM(&htim1);
		return;
	}
	if (V_Hi < V_Hi_recover){
		enable_PWM(&htim1);
	}

	MPPT_counter = MPPT_counter + 1;
	P_Lo = P_Lo + I_Lo * V_Lo ;

	// new idea : PI controller which raises the temp_m_min_I.
	// error: I_Lo-I_Lo_max
	// trying only P first
//		float temp_m_min_I = 0.0;
//	float e_I_lim = 0;
//	float kp_I_lim = 0.01, ki_I_lim=0.01, Ie_I_lim = 0;
	e_I_lim = I_Lo - I_Lo_max;
	Ie_I_lim = Ie_I_lim + e_I_lim * dt;
	if (Ie_I_lim<0) Ie_I_lim = 0;
	temp_m_min_I = e_I_lim * kp_I_lim + Ie_I_lim * ki_I_lim;

//	V_Hi_max
//	float temp_m_min_V = 0.0;
//	float e_V_lim = 0;
//	float kp_V_lim = 0.2, ki_V_lim = 10.0, Ie_V_lim = 0;
	e_V_lim = V_Hi - V_Hi_max;
	Ie_V_lim = Ie_V_lim + e_V_lim * dt;
	if (Ie_V_lim<0) Ie_V_lim = 0;
	temp_m_min_V = e_V_lim * kp_V_lim + Ie_V_lim * ki_V_lim;

	temp_m_min = (temp_m_min_V > temp_m_min_I) ? temp_m_min_V : temp_m_min_I;
//	temp_m_min = max(temp_m_min_V, temp_m_min_I);

	if (m < temp_m_min){
		m  = temp_m_min;
		TIM1->CCR2 = TIM1->ARR * m;
	}

	if (MPPT_counter >= MPPT_eval_count){
//		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_2);
		MPPT_counter = 0;
		if (P_Lo < P_Lo_old){  // got worse, changing direction
			MPPT_dm = -1.0 * MPPT_dm;
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_2);
		}
		P_Lo_old = P_Lo;
		P_Lo = 0;
		m = m + MPPT_dm;
		if (m<m_min) {
			m = 0.5*(m_min+m_max);
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
		}
		if (m>m_max){
			m = 0.5*(m_min+m_max);
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
		}
		// setting PWM
		TIM1->CCR2 = TIM1->ARR * m;
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	canfil.FilterBank = 0;
	canfil.FilterMode = CAN_FILTERMODE_IDMASK;
	canfil.FilterFIFOAssignment = CAN_RX_FIFO0;
	canfil.FilterIdHigh = 0;
	canfil.FilterIdLow = 0;
	canfil.FilterMaskIdHigh = 0;
	canfil.FilterMaskIdLow = 0;
	canfil.FilterScale = CAN_FILTERSCALE_32BIT;
	canfil.FilterActivation = ENABLE;
	canfil.SlaveStartFilterBank = 14;
	txHeader.DLC = 8; // Number of bytes to be transmitted max- 8
	txHeader.IDE = CAN_ID_EXT;
	txHeader.RTR = CAN_RTR_DATA;
	//txHeader.StdId = 0x030;  // the node ID
	txHeader.ExtId = 0x00;
	txHeader.TransmitGlobalTime = DISABLE;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_CAN_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  HAL_CAN_ConfigFilter(&hcan,&canfil); //Initialize CAN Filter
  HAL_CAN_Start(&hcan); //Initialize CAN Bus
  HAL_CAN_ActivateNotification(&hcan,CAN_IT_RX_FIFO0_MSG_PENDING);// Initialize CAN Bus Rx Interrupt
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_buf, N_ADC * N_OVERSAMPLE);

  LoadCalibrationData(&calib_data);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  enable_PWM(&htim1);
//  disable_PWM(&htim1);
  TIM1->CCR2 = TIM1->ARR * 1.0;
  while (1)
  {
	  HAL_Delay(10);
	  CAN_send_2floats(CAN_ANALOG_VALUES_HI << 8 | node_id, &hcan, V_Hi, I_Hi);
	  CAN_send_2floats(CAN_ANALOG_VALUES_LO << 8 | node_id, &hcan, V_Lo, I_Lo);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 4;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC_REGULAR_RANK_4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief CAN Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN_Init(void)
{

  /* USER CODE BEGIN CAN_Init 0 */

  /* USER CODE END CAN_Init 0 */

  /* USER CODE BEGIN CAN_Init 1 */

  /* USER CODE END CAN_Init 1 */
  hcan.Instance = CAN1;
  hcan.Init.Prescaler = 4;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan.Init.TimeSeg1 = CAN_BS1_4TQ;
  hcan.Init.TimeSeg2 = CAN_BS2_3TQ;
  hcan.Init.TimeTriggeredMode = DISABLE;
  hcan.Init.AutoBusOff = DISABLE;
  hcan.Init.AutoWakeUp = DISABLE;
  hcan.Init.AutoRetransmission = DISABLE;
  hcan.Init.ReceiveFifoLocked = DISABLE;
  hcan.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN_Init 2 */

  /* USER CODE END CAN_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 719;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB13 PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */


uint32_t CalculateChecksum(CalibrationData* data) {
    uint32_t checksum = 0;
    uint32_t* data_ptr = (uint32_t*)data;
    // Calculate checksum for the entire data structure except the checksum field itself
    for (uint32_t i = 0; i < (sizeof(CalibrationData) / 4) - 1; i++) {
        checksum ^= data_ptr[i];
    }
    return checksum;
}

bool IsErased(CalibrationData* data) {
    uint8_t* byte_ptr = (uint8_t*)data;
    for (size_t i = 0; i < sizeof(CalibrationData); i++) {
        if (byte_ptr[i] != 0xFF) {
            return false;
        }
    }
    return true;
}

bool IsValidCalibrationData(CalibrationData* data) {
    return data->checksum == CalculateChecksum(data); // && data->version == default_calib_data.version;
}

void LoadCalibrationData(CalibrationData* calib_data) {
    CalibrationData* flash_data = (CalibrationData*)FLASH_USER_START_ADDR;
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13);
//    node_id = 0x02;
    if (IsErased(flash_data)) {
//    	node_id = node_id + 0x01;
    }
    if (!IsValidCalibrationData(flash_data)){
//    	node_id = node_id + 0x10;
    }
    if (IsErased(flash_data) || !IsValidCalibrationData(flash_data)) {
//    	node_id = node_id + 0x20;
        // Use default data and write to flash
//        memcpy(calib_data, &default_calib_data, sizeof(CalibrationData));
//        calib_data->checksum = CalculateChecksum(calib_data);
        WriteCalibrationData(calib_data);
    } else {
        // Load data from flash
//    	node_id = node_id + 0x40;
        memcpy(calib_data, flash_data, sizeof(CalibrationData));
    }

    // Map to readable variables if necessary
    // node_id = calib_data->uint8_ts[0];
    // for (int i = 0; i < 5; i++) {
    //     ADC_offsets[i] = (int16_t)calib_data->uint16_ts[i];
    //     ADC_factors[i] = calib_data->floats[i];
    // }
}

void WriteCalibrationData(CalibrationData *data) {

//	node_id = node_id + 0x40;
	uint32_t Address = FLASH_USER_START_ADDR;
//	CalibrationData* flash_data = (CalibrationData*)FLASH_USER_START_ADDR;
//    memcpy(data, &default_calib_data, sizeof(CalibrationData));
    data->checksum = CalculateChecksum(data);
    // Unlock the Flash to enable the flash control register access
    HAL_FLASH_Unlock();

    // Erase the user Flash area
    FLASH_EraseInitTypeDef EraseInitStruct;
    uint32_t PageError;

    EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.PageAddress = FLASH_USER_START_ADDR;
    EraseInitStruct.NbPages = 1;

    // Disable interrupts
    __disable_irq();

    if (HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) == HAL_OK) {
        // Verify erase
        bool erase_successful = true;
        for (uint32_t addr = FLASH_USER_START_ADDR; addr < FLASH_USER_END_ADDR; addr++) {
            if (*(uint8_t*)addr != 0xFF) {
                erase_successful = false;
                break;
            }
        }

        if (!erase_successful) {
//            node_id = 0xE0;  // Erase verification failed
        } else {
            // Write the data
            uint32_t *data_ptr = (uint32_t *)data;
            for (uint32_t i = 0; i < sizeof(CalibrationData) / 4; i++) {
                if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, data_ptr[i]) != HAL_OK) {
                    node_id = 0xFF;
                    break;
                }
                Address += 4;
            }
        }
    } else {
//        node_id = 0xF0;
    }

    // Enable interrupts
    __enable_irq();

    // Lock the Flash to disable the flash control register access
    HAL_FLASH_Lock();
}



void process_can_set_value(CAN_RxHeaderTypeDef *rxHeader,uint8_t canRX[]){

//	float _m = 0;
	float m_temp = 0;
	switch (canRX[0]){
		case CAN_SET_VALUE_CANID:
			node_id = canRX[1];
			break;
		case CAN_SET_VALUE_PWM_DC:
			if ((state != STATE_FIXED_PWM) & (rxHeader->DLC < 3))
				return;  // only when we are in fixed PWM and the message has at least 5 bytes.
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);// toggle PA0 LED to show we care
			m_temp = (float)(canRX[1] << 8 | canRX[2])/65535.0;
			TIM1->CCR2 = TIM1->ARR * m_temp;
			break;
		case CAN_SET_VALUE_PWM_FREQ:
			// ARR = top -> freq
			// CCR2 = compare -> m
			m_temp = (float)TIM1->CCR2 / (float)TIM1->ARR;
			float f_new = 4.0*(float)(canRX[1] << 8 | canRX[2]);
			uint16_t AAR_new = (uint16_t)(f_pwm_counter / f_new);
			if (AAR_new < TIM1->ARR){ // higher frequency, let's set the new m first
				TIM1->CCR2 = AAR_new * m_temp; // this will lower the mod index but it's still ok
				TIM1->ARR = AAR_new; // now we increase the frequency, everything fine
			}
			else{ // CCR2 will increase, so we first set the new ARR
				TIM1->ARR = AAR_new; // this will effectively lower m
				TIM1->CCR2 = TIM1->ARR * m_temp;
			}
//			disable_PWM(&htim1);
//			TIM1->ARR = AAR_new;
//			TIM1->CCR2 = TIM1->ARR * m;
//			enable_PWM(&htim1);
			break;
	    case CAN_SET_VALUE_CURRENT:
	    	I_Lo_set = (float) ((canRX[1] << 8 | canRX[2]) - 0x7FFF) / 1000.0;
	        break;
	    case CAN_SET_VALUE_VOLTAGE:
			V_Lo_set = (float) ((canRX[1] << 8 | canRX[2])) / 1000.0;
			break;
		default:
			break;
	    }
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan1)
{
//	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13);
	HAL_CAN_GetRxMessage(hcan1, CAN_RX_FIFO0, &rxHeader, canRX); //Receive CAN bus message to canRX buffer
	if (rxHeader.IDE==CAN_ID_STD){ // not dealing with std IDs

		return;	}

	u_int8_t id = rxHeader.ExtId & 0xFF;
	if ((id != 0x00) & (id != node_id)) return;  // does not concern us
	// this message concerns us
	u_int16_t func = ((rxHeader.ExtId >> 8) & 0xFFFF);  // getting 2 bytes for function

	switch (func)
	{
	case CAN_COMMAND:
//		process_can_command(&rxHeader, canRX);
		break;

	case CAN_SET_STATE:
//		process_can_state(id, msg_1);
		break;

	case CAN_SET_VALUE:
		process_can_set_value(&rxHeader, canRX);
		break;

	default:

		break;
	}
//	TIM1->CCR2 = canRX[0];
}

// Called when first half of buffer is filled
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) {
//	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);// toggle PA0 LED
//  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
//	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13);

	memcpy(adc_buf2, adc_buf, sizeof adc_buf); // copying values from adc_buf to adc_buf2
	memset(avg_vals, 0, sizeof avg_vals);  // setting averages to 0

	for (uint8_t i = 0; i<N_OVERSAMPLE; ++i){
		for (uint8_t i2 = 0; i2<N_ADC; ++i2){
			avg_vals[i2] += adc_buf2[i * N_ADC  + i2];
		}
	}

	for (uint8_t i2 = 0; i2<N_ADC; ++i2){
		avg_vals[i2] = (float)avg_vals[i2] / (float)N_OVERSAMPLE;
	}


	//averaging, removing offset, scaling
	for (uint8_t i2 = 0; i2<N_ADC; ++i2){
//		avg_vals[i2] = avg_vals[i2] / (float)N_OVERSAMPLE;
//		avg_val = (float)avg_vals[i2] / (float)N_OVERSAMPLE;
		avg_val = (float)avg_vals[i2];
		offset_corr[i2] = avg_val - (float)ADC_offsets[i2];
		values[i2] = (float) offset_corr[i2] * ADC_factors[i2];
	}

//	V_Hi = adc_buf[0]; // get_V_Hi(values);
//	I_Hi = adc_buf[1]; //get_I_Hi(values);
//	V_Lo = adc_buf[2]; //get_V_Lo(values);
//	I_Lo = adc_buf[3]; //get_I_Lo(values);
	V_Hi =((float)avg_vals[2] - (float) V_Hi_offset) * V_Hi_scale;
//	V_Hi = get_V_Hi(values);
	//I_Hi = get_I_Hi(values);
	I_Hi=((float)avg_vals[3]-(float)I_Hi_offset-(float)avg_vals[2]*I_Hi_offset_scale)
			*I_Hi_scale;

//	V_Lo = get_V_Lo(values);
	V_Lo =((float)avg_vals[0] - (float) V_Lo_offset) * V_Lo_scale;
	I_Lo = ((float)avg_vals[1] - (float) I_Lo_offset - (float)avg_vals[0]*I_Lo_offset_scale)
			* I_Lo_scale;
	//I_Lo = get_I_Lo(values);
//#define I_Lo_offset calib_data.int16_ts[4]
//#define I_Lo_scale calib_data.floats[5]
//#define I_Lo_offset_scale calib_data.floats[6]

//	MPPT();
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {

	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);

  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
