#include "PCB_definitions.h"
#include "can_markers.h"
#include "states.h"




#ifndef SBC_values
	#define SBC_values

	typedef struct {
		uint32_t version;
		uint32_t checksum;
		uint8_t uint8_ts[64];
		int16_t int16_ts[64];
		uint32_t uint32_ts[64];
		float floats[64];
	} __attribute__((aligned(4))) CalibrationData;
	CalibrationData calib_data ={
	    .version = 1,
	    .checksum = 0,  // This will be calculated
	    .uint8_ts = {0xEE},
	    .int16_ts = {-64,-64,1922,1922},
	    .uint32_ts = {0},
	    .floats = {0.017020014840148263,0.01697426428594023,
	    		-0.013030960127851,0.004801406457543891,
//				1,0,
				-0.01299730099815863, 0.00314162406472756}
	};
//	//			int16_t ADC_offsets[4] = 	{-70, 	1920,		-71,		1914};
	//			float ADC_factors[4] 	= {17.03e-3f, -13.71e-3f, 17.01e-3f, -13.43e-3f};

	#define node_id calib_data.uint8_ts[0]

	#define V_Hi_offset calib_data.int16_ts[0]
	#define V_Hi_scale calib_data.floats[0]

	#define V_Lo_offset calib_data.int16_ts[1]
	#define V_Lo_scale calib_data.floats[1]

	#define I_Hi_offset calib_data.int16_ts[2]
	#define I_Hi_scale calib_data.floats[2]
	#define I_Hi_offset_scale calib_data.floats[3]

	#define I_Lo_offset calib_data.int16_ts[3]
	#define I_Lo_scale calib_data.floats[4]
	#define I_Lo_offset_scale calib_data.floats[5]

//	int16_t I_Lo_offset = 1922;
//	float I_Lo_scale = -0.01299730099815863;
//	float I_Lo_offset_scale = 0.00314162406472756;




	uint8_t last_state = STATE_IDLE;
	float dm_max = 0.9, dm_dI = 0.0;
//	float dt = 1.0/11.2e3;  // control dt SBC9
	float dt = 8.0/11.2e3;  // control dt SBC6 (32 oversample)
	//playing around..
//	float VC_kp = 10.0, VC_ki = 0, VC_kd = 0.0;
	#define VC_ku 1.0f  //originally 5.0f
	#define VC_tu 1.0e-3f
	float VC_kp = 0.45*VC_ku, VC_ki = 0.54*VC_ku/VC_tu, VC_kd = 0.0;
//	float VC_kp = 0.6*VC_ku, VC_ki = 1.2*VC_ku/VC_tu, VC_kd = 3*VC_ku*VC_tu/40;
	float V_Hi_max = 56.0, V_Hi_min = 43.0;    // safety (used in current controller
	float V_Hi_shutdown = 58.0;
	float R_int = 0.1;
	float I_Lo_set = -0.5;
	float V_Lo_set = 15.0;


	float m_min = 0.1, m_max = 0.99;
	// MK8: I_Lo, V_Hi, V_Lo, I_Hi
	#ifdef SBCmk8_05
		#include "PCBs/SBC_values_8_5.h"
	#elif defined(SBCmk9_10)
		#include "PCBs/SBCmk9_10.h"
	#elif defined(SBCmk9_12)
		#include "PCBs/SBCmk9_12.h"
	#elif defined(SBCmk9_13)
		#include "PCBs/SBCmk9_13.h"
	#elif defined(SBCmk9_14)
		#include "PCBs/SBCmk9_14.h"
	#elif defined(SBCmk6_14)
			float m = 0.5;
			#define ku 0.01f
			#define tu 0.76e-3f
			float CC_kp = 0.45*ku, CC_ki = 0.54*ku/tu, CC_kd = 0;
	//		float CC_kp = 0.6*ku, CC_ki = 1.2*ku/tu, CC_kd = 3*ku*tu/40;
			#define I_Lo_max 7.0f
			#define V_Lo_LL 20.0f  // Lower Limit for V_Lo for LevelMate
			#define V_Lo_UL 40.0f
			#define V_Lo_min 15.0f
			#define V_Lo_max 450.f
			#define V_Hi_LL 45.0f
			#define V_Hi_UL 50.0f
			uint8_t state = STATE_MPPT;
//			uint8_t node_id = 0xEE;
			// 							V_Hi, 	I_Lo, 	V_Lo, 	I_Hi
//			int16_t ADC_offsets[4] = 	{-70, 	1920,		-71,		1914};
//			float ADC_factors[4] 	= {17.03e-3f, -13.71e-3f, 17.01e-3f, -13.43e-3f};
			int16_t ADC_offsets[4] = 	{0,  0,		0,		0};
			float ADC_factors[4]  = 	{1.0, 1.0, 1.0, 1.0};
			// MPPT
			float temp_m_min_I = 0.0;
			float e_I_lim = 0;
			float kp_I_lim = 0.2, ki_I_lim = 10.0, Ie_I_lim = 0;

			float temp_m_min_V = 0.0;
			float e_V_lim = 0;
			float kp_V_lim = 0.2, ki_V_lim = 1.00, Ie_V_lim = 0;

			float temp_m_min = 0.0;

			float P_Lo_old = 0, P_Lo = 0;
			int16_t MPPT_counter = 0, MPPT_eval_count = 1000;
			float MPPT_dm = -0.01; // step to take when looking for the MPP
			float V_Hi_recover = 54.0; // PWM will be resumed
	#elif defined(SBCmk9eng)  // TODO: Move into separate file
		float m = 0.5;
		#define ku 0.01f
		#define tu 0.76e-3f
		float CC_kp = 0.45*ku, CC_ki = 0.54*ku/tu, CC_kd = 0;
//		float CC_kp = 0.6*ku, CC_ki = 1.2*ku/tu, CC_kd = 3*ku*tu/40;
		#define I_Lo_max 7.0f
		#define V_Lo_LL 20.0f  // Lower Limit for V_Lo for LevelMate
		#define V_Lo_UL 40.0f
		#define V_Lo_min 15.0f
		#define V_Lo_max 450.f
		#define V_Hi_LL 45.0f
		#define V_Hi_UL 50.0f
		uint8_t state = STATE_FIXED_CURR;
		uint8_t node_id = 0xEE;
		// I_Lo V LO IHi Offset VHi
		int16_t ADC_offsets [5] = {1955, 	-52,		1955,		0,		-57};
		float ADC_factors[5] 	= {-0.00774193548387097, 0.0171038017941051, 	0.00783817951959545, 	1.0, 	0.0171248079221444};

		// MPPT
		float temp_m_min_I = 0.0;
		float e_I_lim = 0;
		float kp_I_lim = 0.2, ki_I_lim = 10.0, Ie_I_lim = 0;
		float P_Lo_old = 0, P_Lo = 0;
		int16_t MPPT_counter = 0, MPPT_eval_count = 20000;
		float MPPT_dm = -0.01; // step to take when looking for the MPP
		float V_Hi_recover = 56.0; // PWM will be resumed
	#elif defined(SBCmk9_01)  // TODO: Move into separate file
		#define I_Lo_max 7.0f
		#define V_Lo_LL 20.0f  // Lower Limit for V_Lo for LevelMate
		#define V_Lo_UL 40.0f
		#define V_Lo_min 15.0f
		#define V_Lo_max 450.f
		#define V_Hi_LL 45.0f
		#define V_Hi_UL 50.0f
//		uint8_t state = STATE_FIXED_PWM;
		uint8_t state = STATE_MPPT;
//		V_Lo_set = 28.0;
		uint8_t node_id = 0xCA;
		// I_Lo V LO IHi Offset VHi
		int16_t ADC_offsets [5] = {1955, 	-52,		1955,		0,		-57};
		float ADC_factors[5] 	= {-0.00774193548387097, 0.0171038017941051, 	0.00783817951959545, 	1.0, 	0.0171248079221444};

		// MPPT
		float temp_m_min_I = 0.0;
		float e_I_lim = 0;
		float kp_I_lim = 0.2, ki_I_lim = 10.0, Ie_I_lim = 0;
		float P_Lo_old = 0, P_Lo = 0;
		int16_t MPPT_counter = 0, MPPT_eval_count = 20000;
		float MPPT_dm = -0.01; // step to take when looking for the MPP
		float V_Hi_recover = 56.0; // PWM will be resumed
	#else
		#define I_Lo_max 6.0f
		#define V_Lo_LL 11.0f  // Lower Limit for V_Lo for LevelMate
		#define V_Lo_UL 13.8f
		#define V_Lo_min 10.8f
		#define V_Lo_max 14.4f
		#define V_Hi_LL 45.0f
		#define V_Hi_UL 50.0f
		uint8_t state = STATE_STOPPED;  // use for calibration
		uint8_t node_id = 0xCA;
		int16_t ADC_offsets [5] = {0, 	0,		0,		0,		0};
		float ADC_factors[5] 	= {1.0, 1.0, 	1.0, 	1.0, 	1.0};
	#endif

//	float V_Lo_set = (V_Lo_UL + V_Lo_LL)/2.0;
	float V_Lo_set_int = (V_Lo_UL + V_Lo_LL)/2.0;
	float alpha = (V_Lo_UL- V_Lo_LL)/(V_Hi_UL - V_Hi_LL);

	float I_Hi = 0.0, V_Hi = 0.0, I_Lo = 0.0, V_Lo = 0.0;
	float CC_e = 0.0, CC_Ie = 0.0, CC_De = 0.0, CC_e1 = 0.0;
	float CC_P = 0, CC_I = 0, CC_D = 0;
	float VC_e = 0.0, VC_Ie = 0.0, VC_De = 0.0, VC_e1 = 0.0;
	float VC_P = 0, VC_I = 0, VC_D = 0;
	float f_pwm_counter = 72e6;
	float dm=0.1, m_ff = 0.1,m0 = 0.1; //m_ff -> feed forward = I * dm_dI


#endif
