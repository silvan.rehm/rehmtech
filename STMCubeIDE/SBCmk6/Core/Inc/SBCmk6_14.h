#ifndef SBC_spec_vals
	#define SBC_spec_vals
		CalibrationData calib_data ={
		.version = 1,
		.checksum = 0,  // This will be calculated
		.uint8_ts = {0x23},
		.uint16_ts = {0},
		.uint32_ts = {0},
		.floats = {0.0}
	};
#endif
