#include "stm32f1xx_hal.h"
#include "PCB_definitions.h"

#ifdef SBCmk8

	float get_V_Hi(float values[]){
		return values[1];
	}

	float get_I_Hi(float values[]){
		return values[3];
	}

	float get_V_Lo(float values[]){
		return values[2];
	}

	float get_I_Lo(float values[]){
		return values[0];
	}

	void enable_PWM(){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);  // PWM on
	}

	void disable_PWM(){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);  // PWM off
	}

#endif

#ifdef SBCmk6
	float get_V_Hi(float values[]){
			return values[2];
		}

	float get_I_Hi(float values[]){
			return values[3];
		}

	float get_V_Lo(float values[]){
			return values[0];
		}

	float get_I_Lo(float values[]){
			return values[1];
		}

		void enable_PWM(TIM_HandleTypeDef *htim){
			HAL_TIM_PWM_Start(htim, TIM_CHANNEL_2);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);  // PWM on
		}

		void disable_PWM(TIM_HandleTypeDef *htim){
			HAL_TIM_PWM_Stop(htim, TIM_CHANNEL_2);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);  // PWM off
		}
#endif

#ifdef SBCmk9
	float get_V_Hi(float values[]){
		return values[4];
	}

	float get_I_Hi(float values[]){
		return values[2];
	}

	float get_V_Lo(float values[]){
		return values[1] - values[3];
	}

	float get_I_Lo(float values[]){
		return values[0];
	}

//	void enable_PWM(){
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);  // PWM on
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET); // green LED
//	}
//
//	void disable_PWM(){
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);  // PWM off
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET); // green LED
//	}
//
	void enable_PWM(TIM_HandleTypeDef *htim){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);  // PWM on
		HAL_TIM_PWM_Start(htim, TIM_CHANNEL_2);
		HAL_TIMEx_PWMN_Start(htim, TIM_CHANNEL_2);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET); // green LED
	}

	void disable_PWM(TIM_HandleTypeDef *htim){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);  // PWM off
		HAL_TIM_PWM_Stop(htim, TIM_CHANNEL_2);
		HAL_TIMEx_PWMN_Stop(htim, TIM_CHANNEL_2);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET); // green LED
	}
#endif
