#include "stm32f1xx_hal.h"

void CAN_send_2floats(uint32_t ExtId, CAN_HandleTypeDef* hcan, float float1, float float2) {

    CAN_TxHeaderTypeDef txHeader;
    txHeader.ExtId = ExtId;
    txHeader.RTR = CAN_RTR_DATA;
    txHeader.IDE = CAN_ID_EXT;
    txHeader.DLC = 8;
    uint8_t canTX[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    uint32_t canMailbox;

//    for(int i = 0; i < 4; i++) canTX[i] = ((char*) &float1)[i];
//    for(int i = 0; i < 4; i++) canTX[i+4] = ((char*) &float2)[i];
    memcpy(&canTX[0], &float1, sizeof(float));
    memcpy(&canTX[4], &float2, sizeof(float));

    HAL_CAN_AddTxMessage(hcan, &txHeader, canTX, &canMailbox);
}
