/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  * TESTING TESTING
  * lauft!
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "can_markers.h"
#include "states.h"
#include "stdio.h"
#include <string.h>
#include "PCB_definitions.h"
//#include "PCBs/SBCmk8_05.h"
#include "SBC_values.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define N_ADC 5
#define N_OVERSAMPLE 5
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

CAN_HandleTypeDef hcan;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */
uint16_t adc_buf[N_OVERSAMPLE * N_ADC];
uint16_t adc_buf2[N_OVERSAMPLE * N_ADC];
uint16_t avg_vals[N_ADC];
int16_t offset_corr[N_ADC] = {0};
float values[N_ADC] = {0.0};
uint16_t can_counter = 0;


CAN_RxHeaderTypeDef rxHeader; //CAN Bus Transmit Header
CAN_TxHeaderTypeDef txHeader; //CAN Bus Receive Header
uint8_t canRX[8] = {0,0,0,0,0,0,0,0};  //CAN Bus Receive Buffer
uint8_t canTX[8] = {0,0,0,0,0,0,0,0};  //CAN Bus Transmit Buffer
CAN_FilterTypeDef canfil; //CAN Bus Filter
uint32_t canMailbox; //CAN Bus Mail box variable


uint32_t cnt0 = 0;
uint32_t cnt1 = 0;
uint32_t dcnt = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_CAN_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void process_can_command(CAN_RxHeaderTypeDef *rxHeader,uint8_t canRX[]){
	uint8_t length = rxHeader->DLC;
	if (length < 1){ // no pay load, no fun
		return;}
}

void process_can_set_value(CAN_RxHeaderTypeDef *rxHeader,uint8_t canRX[]){
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_2);
//	float _m = 0;
	float m_temp = 0;
	switch (canRX[0]){
		case CAN_SET_VALUE_PWM_DC:
			if ((state != STATE_FIXED_PWM) & (rxHeader->DLC < 3))
				return;  // only when we are in fixed PWM and the message has at least 5 bytes.
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);// toggle PA0 LED to show we care
			m_temp = (float)(canRX[1] << 8 | canRX[2])/65535.0;
			TIM1->CCR2 = TIM1->ARR * m_temp;
			break;
		case CAN_SET_VALUE_PWM_FREQ:
			// ARR = top -> freq
			// CCR2 = compare -> m
			m_temp = (float)TIM1->CCR2 / (float)TIM1->ARR;
			float f_new = 4.0*(float)(canRX[1] << 8 | canRX[2]);
			uint16_t AAR_new = (uint16_t)(f_pwm_counter / f_new);
			if (AAR_new < TIM1->ARR){ // higher frequency, let's set the new m first
				TIM1->CCR2 = AAR_new * m_temp; // this will lower the mod index but it's still ok
				TIM1->ARR = AAR_new; // now we increase the frequency, everything fine
			}
			else{ // CCR2 will increase, so we first set the new ARR
				TIM1->ARR = AAR_new; // this will effectively lower m
				TIM1->CCR2 = TIM1->ARR * m_temp;
			}
//			disable_PWM(&htim1);
//			TIM1->ARR = AAR_new;
//			TIM1->CCR2 = TIM1->ARR * m;
//			enable_PWM(&htim1);
			break;
	    case CAN_SET_VALUE_CURRENT:
	    	I_Lo_set = (float) ((canRX[1] << 8 | canRX[2]) - 0x7FFF) / 1000.0;
	        break;
	    case CAN_SET_VALUE_VOLTAGE:
			V_Lo_set = (float) ((canRX[1] << 8 | canRX[2])) / 1000.0;
			break;
		default:
			break;
	    }
}


void control_Current(){

//    if (V_Hi >= 2.0)
//    	m0 = V_Lo/V_Hi;
//	else
//		m0 = 0.1;
    m_min = V_Lo/V_Hi_max;
    if (m_min < 0.1) m_min = 0.1;
//    m0 = 0;

    // Limitig current based on voltages
    // too high VDC -> no current into DC Link allowed (this only happens when setting current manually)
//    float I_Bat_min_DC = (V_DC_max - V_Hi)*I_Bat_max/dV_DC * -1.0;
//    I_Bat_min_DC = limit_val(I_Bat_min_DC, -1.0*I_Bat_max, 0.0);
//
//
//
//    // Battery voltage too low -> do not discharge further (same as too high VDC)
//    // float I_Bat_min_Bat = (V_Lo - V_Bat_min)*I_Bat_max/dV_Bat * -1.0;
//    float I_Bat_min_Bat = (V_Bat_int - V_Bat_min)*I_Bat_max/dV_Bat * -1.0;
//    I_Bat_min_Bat = limit_val(I_Bat_min_Bat, -1.0*I_Bat_max, 0.0);
//
//    // Battery voltage too high -> do not charge further
//    // float I_Bat_max_Bat = (V_Lo - V_Bat_max)*I_Bat_max/dV_Bat * -1.0;
//    float I_Bat_max_Bat = (V_Bat_int - V_Bat_max)*I_Bat_max/dV_Bat * -1.0;
//    I_Bat_max_Bat = limit_val(I_Bat_max_Bat, 0.0, I_Bat_max);
//
//    //combine limits
//    float I_Bat_min_lim = max(I_Bat_min_DC, I_Bat_min_Bat);
//    float I_Bat_max_lim = I_Bat_max_Bat;
//
//    float I_Bat_Target_limited = limit_val(I_Bat_Target, I_Bat_min_lim, I_Bat_max_lim);

    //CC_e = I_Bat_Target_limited - I_Lo; // for MPPT
    CC_e = I_Lo - I_Lo_set;
//    CC_e = I_Lo_set - I_Lo;
    // float CC_e = 0, CC_Ie = 0, CC_De = 0;
    CC_Ie = CC_Ie + CC_e * dt;
    CC_De = (CC_e - CC_e1) / dt;
    CC_e1 = CC_e;

    CC_P = CC_kp * CC_e;
    CC_I = CC_ki * CC_Ie;
    if (CC_kd != 0) CC_D = CC_kd * CC_De;
    else CC_D = 0;

    // anti windup, limit CC_Ie to not oversteer m
    if (CC_I > dm_max) CC_Ie = dm_max/CC_ki;
    if (CC_I < -dm_max) CC_Ie = -dm_max/CC_ki;
    CC_I = CC_ki * CC_Ie;
//    if (CC_I > (1.0-CC_P)) CC_Ie = (1.0-CC_P)/CC_ki;
//    if (CC_I < (-0.0-CC_P)) CC_Ie = (-0.0-CC_P)/CC_ki;
//    CC_I = CC_ki * CC_Ie;

//    if ((CC_ki*CC_Ie) > (1.0 - CC_kp*CC_e)) CC_Ie = 1.0/CC_ki;
//    if ((CC_ki * CC_Ie) < -1.0) CC_Ie = -1.0/CC_ki;
    //              don't drag m below 0                dont drag it above 1.0
//    if ((CC_kp * CC_e + CC_ki * CC_Ie) > 1.0) CC_Ie = (1.0 - CC_kp * CC_e) / CC_ki;
//    if ((CC_kp * CC_e + CC_ki * CC_Ie) < 0.0) CC_Ie = -1.0* (CC_ki * CC_Ie) / CC_ki;
//    CC_Ie = max((float)(- 1.0 * m0) / CC_ki, min(CC_Ie, (float)(1.0-m0) / CC_ki));
    //CC_e = I_Lo - I_Lo_Target;
    //CC_Ie = max(0.0, min(CC_Ie + CC_e * dt, 0.95/CC_ki));
    // CC_Ie = max((float)(- 1.0 * m0) / CC_ki, min(CC_Ie + CC_e, (float)(1.0-m0) / CC_ki));

//    dm = P + I + D;
    dm = CC_P + CC_I + CC_D;
//    if (dm >= dm_max)
//    	dm = dm_max;
//    if (dm <= - dm_max)
//    	dm = - dm_max;
    m = m0 + dm_dI * I_Lo_set + dm;
//    m = m0 + dm_dI * I_Lo_set + dm;
//    m = 0.5;

    if (m>0.90) m = 0.90;
    if (m<m_min) m = m_min;

    TIM1->CCR2 = TIM1->ARR * m;
}

void control_Voltage(){
    VC_e = V_Lo - V_Lo_set;  // voltage too low -> negative error
    VC_Ie = VC_Ie + VC_e * dt;
    VC_De = (VC_e - VC_e1) / dt;
    VC_e1 = VC_e;

    VC_P = VC_kp * VC_e;
    VC_I = VC_ki * VC_Ie;
    if (VC_kd != 0) VC_D = VC_kd * VC_De;
    else VC_D = 0;

    // anti windup, limit CC_Ie to not oversteer m
    if (VC_I > I_Lo_max) VC_Ie = I_Lo_max/VC_ki;
    if (VC_I < -I_Lo_max) VC_Ie = -I_Lo_max/VC_ki;
    VC_I = VC_ki * VC_Ie;

    I_Lo_set = VC_P + VC_I + VC_D;
    if (I_Lo_set>I_Lo_max) I_Lo_set = I_Lo_max;
    if (I_Lo_set<-I_Lo_max) I_Lo_set = -I_Lo_max;
}

void level_mate(){
	V_Lo_set_int = V_Lo_LL + alpha * (V_Hi - V_Hi_LL);
	if (V_Lo_set_int > V_Lo_UL) V_Lo_set_int = V_Lo_UL;
	if (V_Lo_set_int < V_Lo_LL) V_Lo_set_int = V_Lo_LL;
	V_Lo_set = V_Lo_set_int - I_Lo * R_int;
	if (V_Lo_set > V_Lo_max) V_Lo_set = V_Lo_max;
	if (V_Lo_set < V_Lo_min) V_Lo_set = V_Lo_min;
}

void limit_voltage(){

	// No delay version:
//	if (V_Hi > V_Hi_max){
//		disable_PWM(&htim1);
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 1);
//	}
//
//	if (V_Hi < V_Hi_recover) {
//		enable_PWM(&htim1);
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);
//	}

//	version with delay:
    static uint32_t disable_timestamp = 0;
    static uint8_t pwm_disabled = 0;
    static uint8_t above_counter = 0; // Counter for consecutive readings above the maximum voltage
    static uint8_t below_counter = 0; // Counter for consecutive readings below the recovery voltage

    if (V_Hi > V_Hi_max) {
        above_counter++;
        below_counter = 0; // Reset the below counter since the value is above the recovery voltage
        if (above_counter >= 3 && !pwm_disabled) { // 3 consecutive values above the max
            disable_PWM(&htim1);
            HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 1);
            disable_timestamp = HAL_GetTick();
            pwm_disabled = 1;
            above_counter = 0; // Reset the counter
        }
    } else if (V_Hi < V_Hi_recover) {
        below_counter++;
        above_counter = 0; // Reset the above counter since the value is below the maximum voltage
        if (below_counter >= 3 && pwm_disabled) {
            uint32_t current_time = HAL_GetTick();
            if ((current_time - disable_timestamp) >= 5000) {
                enable_PWM(&htim1);
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);
                pwm_disabled = 0;
                below_counter = 0; // Reset the counter
            }
        }
    } else {
        // Reset both counters if the value is between the maximum and recovery voltages
        above_counter = 0;
        below_counter = 0;
    }

    if (V_Hi < V_Hi_min){  // if we fall too low we start immediately
    	enable_PWM(&htim1);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);
		pwm_disabled = 0;
    }

}

void MPPT_2(){
	// m0 = 1 - VLo/VHi
	// for a 20V panel at 50V rail -> 0.6 but we are inverse -> 0.4, everything below is "l??ckend"
	// Low m -> more load on the PV -> high m for protecting the f
	// TODO : Limit current
	// TODO: Stop on high voltage
//	limit_voltage();
//
//	if (V_Hi > V_Hi_max){
//		disable_PWM(&htim1);
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 1);
//	}
//
//	else{
//		enable_PWM(&htim1);
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);
//	}

	MPPT_counter = MPPT_counter + 1;
	P_Lo = P_Lo + I_Lo * V_Lo ;

//	//PI controller which raises the temp_m_min_I.
//	//error: I_Lo-I_Lo_max
//	//trying only P first
//	e_I_lim = I_Lo - I_Lo_max;
//	Ie_I_lim = Ie_I_lim + e_I_lim * dt;
//	if (Ie_I_lim<0) Ie_I_lim = 0;
//	temp_m_min_I = e_I_lim * kp_I_lim + Ie_I_lim * ki_I_lim;
//
//	// Over Voltage Limiter
//	e_V_lim = V_Hi - V_Hi_max;
//	Ie_V_lim = Ie_V_lim + e_V_lim * dt;
//	if (Ie_V_lim<0) Ie_V_lim = 0;
//	temp_m_min_V = e_V_lim * kp_V_lim + Ie_V_lim * ki_V_lim;
//
//	temp_m_min = (temp_m_min_V > temp_m_min_I) ? temp_m_min_V : temp_m_min_I;
//
//	if (m < temp_m_min){
//		m  = temp_m_min;
////		TIM1->CCR2 = TIM1->ARR * m;
//	}

	if (MPPT_counter >= MPPT_eval_count){
		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_2);
		MPPT_counter = 0;
		if (P_Lo < P_Lo_old){  // got worse, changing direction
			MPPT_dm = -1.0 * MPPT_dm;
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_2); // green
		}
		P_Lo_old = P_Lo;
		P_Lo = 0;
		m = m + MPPT_dm;
		if (m<m_min) {
			m = 0.5*(m_min+m_max);
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_1); // orange
		}
		if (m>m_max){
			m = 0.5*(m_min+m_max);
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_1); // orange
		}
		// setting PWM

	}
//	__disable_irq();
	TIM1->CCR2 = TIM1->ARR * m;
//	__enable_irq();

}

void MPPT(){
	// Low m -> more load on the PV -> high m for protecting the f
	// TODO : Limit current
	// TODO: Stop on high voltage
	// TODO: I_Lo changed direction -> need to be check current controller
	MPPT_counter = MPPT_counter + 1;
	P_Lo = P_Lo + I_Lo * V_Lo ;

	// new idea : PI controller which raises the temp_m_min.
	// error: I_Lo-I_Lo_max
	// trying only P first
//		float temp_m_min = 0.0;
//	float e_I_lim = 0;
//	float kp_I_lim = 0.01, ki_I_lim=0.01, Ie_I_lim = 0;
	e_I_lim = I_Lo - I_Lo_max;
	Ie_I_lim = Ie_I_lim + e_I_lim * dt;
	if (Ie_I_lim<0) Ie_I_lim = 0;
	temp_m_min = e_I_lim * kp_I_lim + Ie_I_lim * ki_I_lim;

	if (m < temp_m_min){
		m  = temp_m_min;
		TIM1->CCR2 = TIM1->ARR * m;
	}

//	temp_m_min = (I_Lo - I_Lo_max) / dI_Lo_max;
//	if (temp_m_min > m_max) temp_m_min = m_max;
//	if (temp_m_min < m_min) temp_m_min = m_min;
//	if (I_Lo > I_Lo_max){
//		m  = temp_m_min;
//		TIM1->CCR2 = TIM1->ARR * m;
////		// must be m_max when I_Lo is I_Lo_max + dI_LoMax
////		// =  ->
////
//
////		if (m<temp_m_min) m  = temp_m_min;
////
//	}
//	if (V_Hi > V_Hi_max){
//		m = m_max;
//		disable_PWM();
//	}
//	if (V_Hi < V_Hi_recover){
//		enable_PWM();
//	}
	if (MPPT_counter >= MPPT_eval_count){
//		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_2);
		MPPT_counter = 0;
		if (P_Lo < P_Lo_old){  // got worse, changing direction
			MPPT_dm = -1.0 * MPPT_dm;
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_2);
		}
		P_Lo_old = P_Lo;
		P_Lo = 0;
		m = m + MPPT_dm;
		if (m<m_min) {
			m = 0.5*(m_min+m_max);
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
		}
		if (m>m_max){
			m = 0.5*(m_min+m_max);
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
		}
		// setting PWM
		TIM1->CCR2 = TIM1->ARR * m;
	}
}

void do_checks(){
	limit_voltage();
//	if (V_Hi < V_Hi_LL && V_Lo < V_Lo_LL){
//		disable_PWM(&htim1);
//	}
}

void do_states(){

	switch (state)
	{
	case STATE_IDLE:
		if (last_state != state){ // entry action
			last_state = state;
			disable_PWM(&htim1);
		}

		if (target_state!=state){ //we want to go somewhere
			if (V_Hi > V_Hi_LL || V_Lo > V_Lo_LL){  // only if one of the voltages is high enough
				state = target_state;
			}
		}
		break;
	case STATE_STOPPED:
		if (last_state != state){ // entry action
			last_state = state;
			//disable_PWM(&htim1);
			disable_PWM(&htim1);
//			notSD = 0; // shutting down PWM
		}

		break;

	case STATE_AUTOMATIC:
		if (last_state != state){ // entry action
			last_state = state;
//			notSD = 1; // turning on PWM
		}
		break;

	case STATE_FIXED_PWM:
		if (last_state != state){ // entry action
			last_state = state;
			enable_PWM(&htim1);
			TIM1->CCR2 = TIM1->ARR * m;
//			notSD = 1; // turning on PWM
		}
		break;

	case STATE_FIXED_CURR:
		if (last_state != state){ // entry action
			last_state = state;
			enable_PWM(&htim1);
		}
		control_Current();
		break;

	case STATE_FIXED_VOLT:
		if (last_state != state){ // entry action
			last_state = state;
			enable_PWM(&htim1);
		}
		control_Voltage();
		control_Current();
		break;

	case STATE_MPPT:
		if (last_state != state){ // entry action
			last_state = state;
			enable_PWM(&htim1);
		}
		MPPT_2();
//		control_Voltage();
//		control_Current();
		break;
	case STATE_LEVEL_MATE:
		if (last_state != state){ // entry action
			last_state = state;
			enable_PWM(&htim1);
		}
		level_mate();
		control_Voltage();
		control_Current();
		break;

	default:
		break;
	}
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan1)
{
	HAL_CAN_GetRxMessage(hcan1, CAN_RX_FIFO0, &rxHeader, canRX); //Receive CAN bus message to canRX buffer
	if (rxHeader.IDE==CAN_ID_STD){ // not dealing with std IDs
		return;	}

	u_int8_t id = rxHeader.ExtId & 0xFF;
	if ((id != 0x00) & (id != node_id)) return;  // does not concern us

	// this message concerns us
	u_int16_t func = ((rxHeader.ExtId >> 8) & 0xFFFF);  // getting 2 bytes for function

	switch (func)
	{
	case CAN_COMMAND:
		process_can_command(&rxHeader, canRX);
		break;

	case CAN_SET_STATE:
//		process_can_state(id, msg_1);
		break;

	case CAN_SET_VALUE:
		process_can_set_value(&rxHeader, canRX);
		break;

	default:

		break;
	}
//	TIM1->CCR2 = canRX[0];
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	canfil.FilterBank = 0;
	canfil.FilterMode = CAN_FILTERMODE_IDMASK;
	canfil.FilterFIFOAssignment = CAN_RX_FIFO0;
	canfil.FilterIdHigh = 0;
	canfil.FilterIdLow = 0;
	canfil.FilterMaskIdHigh = 0;
	canfil.FilterMaskIdLow = 0;
	canfil.FilterScale = CAN_FILTERSCALE_32BIT;
	canfil.FilterActivation = ENABLE;
	canfil.SlaveStartFilterBank = 14;
	txHeader.DLC = 8; // Number of bites to be transmitted max- 8
	txHeader.IDE = CAN_ID_EXT;
	txHeader.RTR = CAN_RTR_DATA;
	//txHeader.StdId = 0x030;  // the node ID
	txHeader.ExtId = node_id;
	txHeader.TransmitGlobalTime = DISABLE;

	//scaling factors with ADC_factors[i2] / (float)N_OVERSAMPLE
	for (uint32_t i=0; i<N_ADC; ++i){
		ADC_factors[i] = ADC_factors[i] / (float)N_OVERSAMPLE;
		ADC_offsets[i] = ADC_offsets[i] * N_OVERSAMPLE;
	}
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_CAN_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
//  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
//  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_buf, N_ADC * N_OVERSAMPLE);
//  TIM1->CCR2 = 100;
  HAL_TIM_Base_Start(&htim2);
//  enable_PWM(&htim1);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
//  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
  HAL_CAN_ConfigFilter(&hcan,&canfil); //Initialize CAN Filter
  HAL_CAN_Start(&hcan); //Initialize CAN Bus
  HAL_CAN_ActivateNotification(&hcan,CAN_IT_RX_FIFO0_MSG_PENDING);// Initialize CAN Bus Rx Interrupt

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  HAL_Delay(100);
//	  HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_1);
//	  uint8_t csend[] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08}; // Tx Buffer
//	  HAL_CAN_AddTxMessage(&hcan,&txHeader,csend,&canMailbox); // Send Message
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 5;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = ADC_REGULAR_RANK_4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = ADC_REGULAR_RANK_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief CAN Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN_Init(void)
{

  /* USER CODE BEGIN CAN_Init 0 */

  /* USER CODE END CAN_Init 0 */

  /* USER CODE BEGIN CAN_Init 1 */

  /* USER CODE END CAN_Init 1 */
  hcan.Instance = CAN1;
  hcan.Init.Prescaler = 4;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan.Init.TimeSeg1 = CAN_BS1_5TQ;
  hcan.Init.TimeSeg2 = CAN_BS2_3TQ;
  hcan.Init.TimeTriggeredMode = DISABLE;
  hcan.Init.AutoBusOff = DISABLE;
  hcan.Init.AutoWakeUp = DISABLE;
  hcan.Init.AutoRetransmission = DISABLE;
  hcan.Init.ReceiveFifoLocked = DISABLE;
  hcan.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN_Init 2 */

  /* USER CODE END CAN_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1125;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 32;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA3 PA5 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB13 PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

// Called when first half of buffer is filled
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) {
//	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);// toggle PA0 LED
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);
}

// Called when buffer is completely filled
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	cnt0 = TIM2->CNT ; // TIMER START
	memcpy(adc_buf2, adc_buf, sizeof adc_buf); // copying values
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
	can_counter += 1;

	memset(avg_vals, 0, sizeof avg_vals);

	for (uint8_t i = 0; i<N_OVERSAMPLE; ++i){
		for (uint8_t i2 = 0; i2<N_ADC; ++i2){
			avg_vals[i2] += adc_buf2[i * N_ADC  + i2];
		}
	}

	//averaging, removing offset, scaling
	for (uint8_t i2 = 0; i2<N_ADC; ++i2){
		offset_corr[i2] = avg_vals[i2] - ADC_offsets[i2];
		values[i2] = (float) offset_corr[i2] * ADC_factors[i2];
	}

	V_Hi = get_V_Hi(values);
	I_Hi = get_I_Hi(values);
	V_Lo = get_V_Lo(values);
	I_Lo = get_I_Lo(values);

	do_checks();

	do_states();

	cnt1 = TIM2->CNT; // TIMER END
	dcnt = 0;
	if (cnt1<cnt0) // we  had a roll-over
		dcnt = 65535-cnt0+cnt1;
	else
		dcnt = cnt1-cnt0;

	if (can_counter >= 2000){
		  //HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_1);
		  HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13);

//		  uint16_t value = avg_vals[0];
//
//		  canTX[1]=value & 0xff;
//		  canTX[0]=(value >> 8);
//
//		  //value = avg_vals[1] - avg_vals[3];
//		  value = offset_corr[0];
////		  uint8_t arr2[2];
//		  canTX[3]=value & 0xff;
//		  canTX[2]=(value >> 8);
//
//		  value = avg_vals[2];
////		  uint8_t arr3[2];
//		  canTX[5]=value & 0xff;
//		  canTX[4]=(value >> 8);
//
//		  value = avg_vals[4];
////		  uint8_t arr4[2];
//		  canTX[7]=value & 0xff;
//		  canTX[6]=(value >> 8);
//
//		  canTX[7] = dcnt & 0xff;
//		  canTX[6] = (dcnt >>8) & 0xff;
//		  canTX[5] = (dcnt >>16) & 0xff;
//		  canTX[4] = (dcnt >>24) & 0xff;

		  // Initialize canTX array with zeros
		  uint8_t canTX[8] = {0};
		  uint16_t pwmCCR2Value = TIM1->CCR2;
		  uint16_t pwmARRValue = TIM1->ARR;

		  canTX[1] = pwmCCR2Value & 0xff;
		  canTX[0] = (pwmCCR2Value >> 8) & 0xff;

		  canTX[3] = pwmARRValue & 0xff;
		  canTX[2] = (pwmARRValue >> 8) & 0xff;

		  canTX[4] = state;

		  txHeader.ExtId = CAN_INTERNAL_STATE_0 << 8 | node_id;
		  HAL_CAN_AddTxMessage(&hcan,&txHeader,canTX,&canMailbox); // Send Message

		  // analog Values
		  for(int i = 0; i < 4; i++) canTX[i] = ((char*) &V_Hi)[i];
		  for(int i = 0; i < 4; i++) canTX[i+4] = ((char*) &I_Hi)[i];
//		  for(int i = 0; i < 4; i++) canTX[i] = ((char*) &P_Lo_old)[i];
//		  for(int i = 0; i < 4; i++) canTX[i+4] = ((char*) &temp_m_min)[i];
		  txHeader.ExtId = CAN_ANALOG_VALUES_HI << 8 | node_id;
		  HAL_CAN_AddTxMessage(&hcan,&txHeader,canTX,&canMailbox); // Send Message

		  for(int i = 0; i < 4; i++) canTX[i] = ((char*) &V_Lo)[i];
		  for(int i = 0; i < 4; i++) canTX[i+4] = ((char*) &I_Lo)[i];
//		  for(int i = 0; i < 4; i++) canTX[i] = ((char*) &CC_e)[i];
//		  for(int i = 0; i < 4; i++) canTX[i+4] = ((char*) &CC_Ie)[i];
		  txHeader.ExtId = CAN_ANALOG_VALUES_LO << 8 | node_id;
		  HAL_CAN_AddTxMessage(&hcan,&txHeader,canTX,&canMailbox); // Send Message

	  can_counter = 0;
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
