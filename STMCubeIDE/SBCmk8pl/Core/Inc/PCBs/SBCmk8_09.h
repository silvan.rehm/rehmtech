#define SBCmk8
#include "SBCmk8_func.h"

uint8_t node_id = 0x29;
//uint8_t state = STATE_STOPPED;  // use for calibration
uint8_t state = STATE_FIXED_CURR;
uint8_t last_state = STATE_FIXED_PWM;

#define I_Lo_max 15.0f
#define V_Lo_LL 24.0f  // Lower Limit for V_Lo for LevelMate
#define V_Lo_UL 32.0f
#define V_Hi_LL 45.0f
#define V_Hi_UL 50.0f

float dm_max = 0.9, dm_dI = 0.0;
float dt = 1.0/11.2e3;  // control dt
float CC_kp = 4.5e-3, CC_ki = 10.8, CC_kd = 0.0;

// Kp is depending on the voltage range of the battery
// goal: full current at 10% delta
float VC_kp = 10.0*I_Lo_max/(V_Lo_UL - V_Lo_LL), VC_ki = 0.0, VC_kd = 0.0;

float V_Hi_max = 59, V_Hi_min = 43;    // safety (used in current controller)

float I_Lo_set = 0.0;
float V_Lo_set = (V_Lo_UL + V_Lo_LL)/2.0;
float alpha = (V_Lo_UL- V_Lo_LL)/(V_Hi_LL - V_Hi_UL);

// Channels: I_Lo, V_Hi, VLo, IHi
int16_t ADC_offsets [5] ={ // must be same length as N_ADC!
		1935,-0x000,0x00,1920,0};
float ADC_factors[5] = {
		-8.92/685.0, //I_Lo
		29.32/1660.0, //V_Hi
		29.77/1685.0, //V_Lo
		-8.92/675.0, //I_Hi
		1.0}; //N/A

//int16_t ADC_offsets [5] ={ // must be same length as N_ADC!
//		0,-0x000,0x00,0,0};
//float ADC_factors[5] = {
//		-1.0, //I_Lo
//		1.0, //V_Hi
//		1.0, //V_Lo
//		-1.0, //I_Hi
//		1.0}; //N/A

float I_Hi = 0.0, V_Hi = 0.0, I_Lo = 0.0, V_Lo = 0.0;
float CC_e = 0.0, CC_Ie = 0.0, CC_De = 0.0, CC_e1 = 0.0;
float CC_P = 0, CC_I = 0, CC_D = 0;
float VC_e = 0.0, VC_Ie = 0.0, VC_De = 0.0, VC_e1 = 0.0;
float VC_P = 0, VC_I = 0, VC_D = 0;
float f_pwm_counter = 72e6;
float m = 0.1, dm=0.1, m_ff = 0.1,m0 = 0.1; //m_ff -> feed forward = I * dm_dI
float m_min = 0.1;
