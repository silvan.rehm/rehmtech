#include "SBCmk9_func.h"

uint8_t node_id = 0xFF;
//uint8_t state = STATE_FIXED_CURR;
uint8_t state = STATE_STOPPED;
uint8_t last_state = STATE_IDLE;

#define I_Lo_max 15.0f
#define V_Lo_LL 24.0f  // Lower Limit for V_Lo for LevelMate
#define V_Lo_UL 32.0f
#define V_Hi_LL 45.0f
#define V_Hi_UL 50.0f

float dm_max = 0.9, dm_dI = 0.0;
float dt = 1.0/11.2e3;  // control dt
float V_Hi_max = 59, V_Hi_min = 43;    // safety (used in current controller)

float I_Lo_set = 0.0;
float V_Lo_set = (V_Lo_UL + V_Lo_LL)/2.0;
float alpha = (V_Lo_UL- V_Lo_LL)/(V_Hi_LL - V_Hi_UL);

// Channels: I_Lo, V_Lo*, I_Hi, GND_Lo, V_Hi
int16_t ADC_offsets [5] ={ // must be same length as N_ADC!
		0, 0, 0, 0, 0};
float ADC_factors[5] = {
		1.0, //I_Lo
		1.0, //V_Lo*
		1.0, //I_Hi
		1.0, //GND_Lo
		1.0}; //V_Hi

//int16_t ADC_offsets [5] ={ // must be same length as N_ADC!
//		0, 0, 0, 0, 0};
//float ADC_factors[5] = {
//		1.0, //I_Lo
//		1.0, //V_Lo*
//		1.0, //I_Hi
//		1.0, //GND_Lo
//		1.0}; //V_Hi
//
//int16_t ADC_offsets [5] ={ // must be same length as N_ADC!
//		0x7A1,-0x03b,0x7A1,0x000,-0x03b};
//float ADC_factors[5] = {
//		0.008058608058608059 / 1.025, //I_Lo
//		0.016923076923076923, //V_Lo*
//		0.008058608058608059 / 1.025, //I_Hi
//		0.0008058608058608059, //GND_Lo
//		0.016923076923076923/0.99}; //V_Hi

// Kp is depending on the voltage range of the battery
// goal: full current at 10% delta
float VC_kp = 10.0*I_Lo_max/(V_Lo_UL - V_Lo_LL), VC_ki = 0.0, VC_kd = 0.0;

//float CC_kp = 0.12, CC_ki = 156, CC_kd = 23.08e-6;
//float CC_kp = 0.09, CC_ki = 70.2, CC_kd = 0.0;
float CC_kp = 0.01, CC_ki = 7.0, CC_kd = 0.0;


float I_Hi = 0.0, V_Hi = 0.0, I_Lo = 0.0, V_Lo = 0.0;
float CC_e = 0.0, CC_Ie = 0.0, CC_De = 0.0, CC_e1 = 0.0;
float CC_P = 0, CC_I = 0, CC_D = 0;
float VC_e = 0.0, VC_Ie = 0.0, VC_De = 0.0, VC_e1 = 0.0;
float VC_P = 0, VC_I = 0, VC_D = 0;
float f_pwm_counter = 72e6;
float m = 0.1, dm=0.1, m_ff = 0.1,m0 = 0.1; //m_ff -> feed forward = I * dm_dI
float m_min = 0.1;
