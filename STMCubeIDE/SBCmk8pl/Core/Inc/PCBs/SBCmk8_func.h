
#ifdef SBCmk8
	float get_V_Hi(float values[]);
	float get_I_Hi(float values[]);
	float get_V_Lo(float values[]);
	float get_I_Lo(float values[]);

	void enable_PWM();
	void disable_PWM();
#endif
