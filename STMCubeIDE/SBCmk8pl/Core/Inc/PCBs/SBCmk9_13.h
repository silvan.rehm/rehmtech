
float m = 0.5;

#define ku 0.005f
#define tu 0.76e-3f
//float CC_kp = 0.45*ku, CC_ki = 0.54*ku/tu, CC_kd = 0;
float CC_kp = 0.6*ku, CC_ki = 1.2*ku/tu, CC_kd = 3*ku*tu/40;
#define I_Lo_max 6.0f
#define V_Lo_LL 24.0f  // Lower Limit for V_Lo for LevelMate
#define V_Lo_UL 32.0f
#define V_Lo_min 10.0f
#define V_Lo_max 35.f
#define V_Hi_LL 45.0f
#define V_Hi_UL 50.0f
uint8_t state = STATE_LEVEL_MATE;

uint8_t node_id = 0x9D;
// 							I_Lo 		V LO 		IHi 		Offset 	VHi
int16_t ADC_offsets [5] = {	1943, 		-57,			1943,		0,		-57};
float ADC_factors[5] 	= {	-7.790E-03, 	17.089E-03, 7.789E-03, 	0.0, 	17.158E-03};

// MPPT
float temp_m_min = 0.0;
float e_I_lim = 0;
float kp_I_lim = 0.2, ki_I_lim = 10.0, Ie_I_lim = 0;
float P_Lo_old = 0, P_Lo = 0;
int16_t MPPT_counter = 0, MPPT_eval_count = 20000;
float MPPT_dm = -0.01; // step to take when looking for the MPP
float V_Hi_recover = 56.0; // PWM will be resumed
