#define I_Lo_max 6.0f
#define V_Lo_LL 11.0f  // Lower Limit for V_Lo for LevelMate
#define V_Lo_UL 13.8f
#define V_Lo_min 10.8f
#define V_Lo_max 14.4f
#define V_Hi_LL 45.0f
#define V_Hi_UL 50.0f
uint8_t node_id = 0x25;
//uint8_t state = STATE_STOPPED;
uint8_t state = STATE_LEVEL_MATE;
int16_t ADC_offsets [5] ={ 1920, -56,-80,1907,0};
float ADC_factors[5] = {
		-8.19/630, //I_Lo
		0.0168918918918919, // 29.39/1678, //V_Hi
		0.004005, //V_Lo
		-8.19/623, //I_Hi
		1.0}; //N/A
