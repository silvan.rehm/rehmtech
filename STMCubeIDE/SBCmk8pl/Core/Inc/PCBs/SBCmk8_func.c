#include "stm32f1xx_hal.h"
//#include "PCB_definitions.h"

#ifdef SBCmk8

	float get_V_Hi(float values[]){
		return values[1];
	}

	float get_I_Hi(float values[]){
		return values[3];
	}

	float get_V_Lo(float values[]){
		return values[2];
	}

	float get_I_Lo(float values[]){
		return values[0];
	}

	void enable_PWM(){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);  // PWM on
	}

	void disable_PWM(){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);  // PWM off
	}

#endif
