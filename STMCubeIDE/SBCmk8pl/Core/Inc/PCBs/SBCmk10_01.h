
float m = 0.5;

#define ku 0.05f
#define tu 0.76e-3f
float CC_kp = 0.01f, CC_ki = 0.0f, CC_kd = 0.0f;
//float CC_kp = 0.45*ku, CC_ki = 0.54*ku/tu, CC_kd = 0;
//float CC_kp = 0.6*ku, CC_ki = 1.2*ku/tu, CC_kd = 3*ku*tu/40;
#define I_Lo_max 8.0f
#define I_Lo_critical 20.0f
#define V_Lo_LL 24.8f  // Lower Limit for V_Lo for LevelMate
#define V_Lo_UL 27.2f
#define V_Lo_min 10.0f
#define V_Lo_max 35.f
#define V_Hi_LL 49.0f
#define V_Hi_UL 51.0f

//float V_Lo_avg = (V_Lo_LL + V_Lo_UL) / 2.0f;
//float V_Hi_avg = (V_Hi_LL + V_Hi_UL) / 2.0f;
float m0 = (V_Lo_LL + V_Lo_UL) / (V_Hi_LL + V_Hi_UL);
float I_Lo_set = -0.0;
//uint8_t state = STATE_LEVEL_MATE;
//uint8_t state = STATE_FIXED_PWM;
uint8_t state = STATE_MPPT;
//uint8_t target_state = STATE_MPPT;
uint8_t target_state = STATE_FIXED_PWM;

uint8_t node_id = 0xB5;
// 							VHi 		ILo 		IHi 		VLo				N/A
//int16_t ADC_offsets [5] = {	0, 		0,		0,		0,			0};
//float ADC_factors[5] 	= {	1.0, 1.0,     1.0, 		1.0, 	0.0};

int16_t ADC_offsets [5] = {	-50, 		1960,		1966,		-57,			0};
float ADC_factors[5] 	= {	17.049E-03, -7.8e-3,     -7.8e-3, 		17.044e-3, 	0.0};

//int16_t ADC_offsets [5] = {	1943, 		-57,			1943,		0,		-57};
//float ADC_factors[5] 	= {	-7.790E-03, 	17.089E-03, 7.789E-03, 	0.0, 	17.158E-03};

// MPPT
float temp_m_min_I = 0.0;
float e_I_lim = 0;
float kp_I_lim = 0.2, ki_I_lim = 2.0, Ie_I_lim = 0;

float temp_m_min_V = 0.0;
float e_V_lim = 0;
float kp_V_lim = 0.2, ki_V_lim = 1.00, Ie_V_lim = 0;

float temp_m_min = 0.0; // for combining minV / minI

float P_Lo_old = 0, P_Lo = 0;
int16_t MPPT_counter = 0, MPPT_eval_count = 6400;
float MPPT_dm = -0.005; // step to take when looking for the MPP

