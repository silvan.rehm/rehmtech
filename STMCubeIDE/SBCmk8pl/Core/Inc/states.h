#ifndef states_h
	#define states_h
	#define STATE_STOPPED       0x00
	#define STATE_ERROR         0xFF
	#define STATE_IDLE          0x20
	#define STATE_AUTOMATIC     0x21
	#define STATE_FIXED_VOLT    0x22
	#define STATE_FIXED_CURR    0x23
	#define STATE_FIXED_PWM     0x24
	#define STATE_MPPT          0x25
	#define STATE_LEVEL_MATE    0x26
#endif
