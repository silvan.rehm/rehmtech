#ifndef can_markers
	#define can_markers
	#define CAN_COMMAND             0x0000

	#define CAN_COMMAND_START       0xFF
	#define CAN_COMMAND_STOP        0x00

	#define CAN_SET_STATE           0x0010

	#define CAN_SET_VALUE           0x0020

	#define CAN_SET_VALUE_PWM_DC    0x10
	#define CAN_SET_VALUE_PWM_FREQ  0x11
	#define CAN_SET_VALUE_CURRENT   0x20
	#define CAN_SET_MAX_CURRENT     0x21
	#define CAN_SET_VALUE_VOLTAGE   0x30

	#define CAN_SET_VALUE_CANID     0x40
	#define CAN_SET_VALUE_A_V_HI    0x41
	#define CAN_SET_VALUE_A_I_HI    0x42
	#define CAN_SET_VALUE_B_I_HI    0x43

	#define CAN_ANALOG_VALUES_HI    0xAA00
	#define CAN_ANALOG_VALUES_LO    0xAA01

	#define CAN_INTERNAL_STATE_0    0xAA80

	#define CAN_INTERNAL_DEBUG      0xAAAA
#endif

