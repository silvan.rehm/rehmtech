################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Inc/PCBs/SBC_func.c \
../Core/Inc/PCBs/SBCmk8_func.c 

OBJS += \
./Core/Inc/PCBs/SBC_func.o \
./Core/Inc/PCBs/SBCmk8_func.o 

C_DEPS += \
./Core/Inc/PCBs/SBC_func.d \
./Core/Inc/PCBs/SBCmk8_func.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Inc/PCBs/%.o Core/Inc/PCBs/%.su Core/Inc/PCBs/%.cyclo: ../Core/Inc/PCBs/%.c Core/Inc/PCBs/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Inc-2f-PCBs

clean-Core-2f-Inc-2f-PCBs:
	-$(RM) ./Core/Inc/PCBs/SBC_func.cyclo ./Core/Inc/PCBs/SBC_func.d ./Core/Inc/PCBs/SBC_func.o ./Core/Inc/PCBs/SBC_func.su ./Core/Inc/PCBs/SBCmk8_func.cyclo ./Core/Inc/PCBs/SBCmk8_func.d ./Core/Inc/PCBs/SBCmk8_func.o ./Core/Inc/PCBs/SBCmk8_func.su

.PHONY: clean-Core-2f-Inc-2f-PCBs

